# Keypoint-based Visual Imitation Learning

[Project Website](https://sites.google.com/view/k-vil) 

ArXiv Preprint: coming soon

---
**Abstract:**

Visual imitation learning provides efficient and intuitive solutions for robotic 
systems to acquire novel manipulation skills. However, simultaneously learning 
geometric task constraints and control policies from visual inputs alone remains 
a challenging problem. In this paper, we propose an approach for keypoint-based 
visual imitation (K-VIL) that automatically extracts sparse, object-centric, and 
embodiment-independent task representations from a small number of human 
demonstration videos. The task representation is composed of keypoint-based 
geometric constraints on principal manifolds, their associated local frames, 
and the movement primitives that are then needed for the task execution.  Our 
approach is capable of extracting such task representations from a single 
demonstration video, and of incrementally updating them when new demonstrations 
become available. To reproduce manipulation skills using the learned set of 
prioritized geometric constraints in novel scenes, we introduce a novel 
keypoint-based admittance controller. We evaluate our approach in several 
real-world applications, showcasing its ability to deal with cluttered scenes, 
new instances of categorical objects, and large object pose and shape variations, 
as well as its efficiency and robustness in both one-shot and few-shot imitation 
learning settings.

---
## Installation

clone this repository
```shell
git clone https://gitlab.com/jianfenggaobit/kvil_public.git
cd kvil_public
```

create virtual environment and install dependencies
```shell
conda create -n kvil python=3.8
conda activate kvil
pip install -e .
```

## Run the experiments

The human demonstration videos of each task are already converted to trajectories of candidate 
points. For each of the real-world experiments listed in our paper, i.e. 
PressButton (`pb`), FetchTissue (`ft`), InsertStick (`is`), PourWater (`pw`), HangHat (`hh`), 
the corresponding data required by K-VIL are stored in `kvil_public/data/demo/real/{EXP}{number_of_demo)`.
E.g. `ft1` means "fetch tissue task with a single demonstration".

To run K-VIL for task FetchTissue `ft1`, simply execute the following command:
```shell
python run.py -d real -p ft1 -v -f
```
The extracted keypoints, geometric constraints, local frames and master-slave 
relationship will be printed to the terminal. And the following visualization 
GUI pops up showing the extracted information as well.

- Drag slider `C` to see different geometric constraints; 
- Drag slider `T` to see the motion of the keypoints and their corresponding slave object;
- Slider `J_i` controls the index of the local frames on the master object;
- Slider `n_kpts` controls the index of the candidate points on the slave object;
- Slider `N` controls which demonstration to visualize. set to `0` to visualize all.

![image](./data/asset/ft1_vis.png)

A complete description of how to use this script is listed below.
```shell
$ python run.py -h
Usage: run.py [OPTIONS]

Options:
  -d, --domain [real|toy]   domain of the evaluation
  -p, --path TEXT           the path to demonstration recordings
  -v, --viz                 whether to visualize constraints
  -vd, --viz_debug          whether to visualize intermediate results
  -f, --force_redo          whether to force redo everything
  -l, --load                whether to load results
  -t, --time_steps INTEGER  how many time steps do you want to visualize
  -h, --help                Show this message and exit.
```

Note that:
- you only need `-f` for the first time, the preprocessed data is stored
locally. 
- To run Principal Constraint Estimation of K-VIL again and then visualize
the results, use `-v`. 
- If you only need to visualize the results again, use `-v -l`.
- Since K-VIL is only applied to extract the keypoints and geometric constraints
of a task at the last time-step, the argument `-t` only affects how many time-steps
to be visualized, i.e. slider `T`. The default value is 2, meaning only the first and 
last time step is visualized. Increase it, e.g. 2 < t <= 20, to visualize a smoother trajectory with 
more time steps.

The motion trajectories `--` are 
used to train Via-point Movement Primitives, the weights can be found in 
`kvil_public/data/demo/real/ft1/constraints/constraint_xx.yaml`, which also 
contains information required by the real-time keypoint-based admittance 
controller (KAC). 

The KAC is part of [ArmarX Robot 
Development Environment](https://gitlab.com/ArmarX), the code example can be found in the
[`armarx/control` repository](https://gitlab.com/ArmarX/skills/control.git)
in branch `new_mp_controller`, 
e.g. `source/armarx/control/njoint_mp_controller/task_space/KVILImpedanceController.h`.

### Toy problem in 2D

This part is not included in our paper. However, this may still help to understand 
the concept of K-VIL. Add `-f` for the first time execution.

#### 2D Pour Water Task
```shell
python run.py -d toy -p pw1 -v
python run.py -d toy -p pw3 -v
python run.py -d toy -p pw6 -v
python run.py -d toy -p pw11 -v
```
You can also use `-vd` option to visualize more intermediate steps. e.g.
local frame matching when the objects are spatially scaled.

### Real world experiments

You can use the following commands to get similar results as presented in our paper 
for the 5 real-world tasks. Add `-f` for the first time execution.

#### Press Button Task

```shell
python run.py -d real -p pb1 -v
python run.py -d real -p pb3 -v
python run.py -d real -p pb4 -v
```
#### Fetch Tissue Task

```shell
python run.py -d real -p ft1 -v
python run.py -d real -p ft3 -v
python run.py -d real -p ft4 -v
python run.py -d real -p ft6 -v
```

#### Insert Stick Task

```shell
python run.py -d real -p is1 -v
python run.py -d real -p is3 -v
python run.py -d real -p is6 -v
```

#### Pour Water Task

```shell
python run.py -d real -p pw1 -v
python run.py -d real -p pw3 -v
python run.py -d real -p pw4 -v
python run.py -d real -p pw11 -v
```

#### Hang Hat Task

```shell
python run.py -d real -p hh1 -v
python run.py -d real -p hh3 -v
python run.py -d real -p hh5_mix_length -v
python run.py -d real -p hh5_mix_shape -v
```

