import cv2
import numpy as np
from math import *
from os.path import join
from scipy.spatial.distance import cdist
from matplotlib.patches import Polygon

from vil.utils import get_vil_path
from vil.utils.py.utils import load_dict_from_yaml


class Tool:
    def __init__(self, name=None, role="Tool", scale: list = None):
        if name is None:
            raise ValueError(f"{name} is invalid")
        self.name = name
        cfg = load_dict_from_yaml(join(get_vil_path("toy"), f"tools/cfg/{name}.yaml"))
        pts_list = cfg["polygon"]
        self.sample_pts = np.array(pts_list + cfg["sample"]) * 10
        # self.sample_pts = np.array(cfg["sample"]) * 10
        self.sample_pts = np.hstack((self.sample_pts, np.ones((self.sample_pts.shape[0], 1))))
        self.n_sample_pts = self.sample_pts.shape[0]
        self.n_polygon_vertices = len(pts_list)
        if len(pts_list) == 0:
            raise ValueError(f"please check whether {name} exists in cfg.yaml")
        self.init_pts = np.array(pts_list) * 10
        # https://stackoverflow.com/questions/31667070/max-distance-between-2-points-in-a-data-set-and-identifying-the-points
        self.spatial_scale = cdist(self.init_pts, self.init_pts, metric='euclidean').max()
        ic(self.spatial_scale)
        self.pts = self.init_pts.copy()
        self.current_scale = np.array(scale) if scale is not None else np.array([1., 1.])

        if role == 'Target':
            self.color = (0, 255, 0)
        elif role == 'Tool':
            self.color = (0, 0, 255)
        else:
            self.color = (255, 0, 0)

    def reset(self):
        self.current_scale = np.array([1., 1.])

    def scale(self, scale: np.ndarray):
        """ scale is a scaling vector with 2 elements """
        self.current_scale = scale
        self.pts = self.init_pts * np.expand_dims(scale, 0)

    def plot_coords(self, ax, color='cyan', zorder=1, alpha=1):
        line, = ax.plot(self.pts[:, 0], self.pts[:, 1], 'o', color=color, zorder=zorder, alpha=alpha)
        return line

    def plot_patch(self, ax, pts=None, face_color='paleturquoise', edge_color="cyan",
                   zorder=-1, alpha=0.4, linewidth=3, linestyle="-"):
        if pts is None:
            pts = self.pts
        polygon = Polygon(pts, closed=True, alpha=alpha, zorder=zorder, facecolor=face_color,
                          edgecolor=edge_color, linewidth=linewidth, linestyle=linestyle)
        return ax.add_patch(polygon)

    def update(self, canvas, rotation, translation):
        """
        transform the polygon by 'rotation' and 'translation' w.r.t. the origin.
        Args:
            canvas: background
            rotation: rotation angle in degrees
            translation: translation vector 2D

        Returns: transformed points and its uv coordinates in the current scene
        """
        rot_mat = np.array([
            [cos(radians(rotation)), -sin(radians(rotation))],
            [sin(radians(rotation)), cos(radians(rotation))]
        ])
        xy_pts = rot_mat.dot(self.pts.copy().T).astype(int).T + translation
        uv_pts = xy_pts.copy()
        uv_pts[:, 1] = -uv_pts[:, 1] + canvas.shape[0]
        return xy_pts, uv_pts.astype(int)

    def draw(self, canvas, uv_pts):
        cv2.polylines(canvas, [uv_pts], True, self.color, 1, 1)

    def get_shape(self):
        """
        return the pts' coordinates after scaling
        """
        return self.pts

    def get_len(self):
        return len(self.pts)

    def get_sample_point_coordinates(self):
        return self.sample_pts[:, :2]

    def get_scaled_sample_point_coordinates(self, n_sample_pts_max=1e3):
        if self.sample_pts.shape[0] > n_sample_pts_max:
            down_sample_idx = np.linspace(0, self.sample_pts.shape[0]-1, n_sample_pts_max).astype(int)
            return self.sample_pts[down_sample_idx, :2] * self.current_scale
        return self.sample_pts[:, :2] * self.current_scale

    def get_local_sample_point_traj(self, scene_config_file, global_traj):
        scene_config = np.load(scene_config_file)
        target_scale = scene_config["target_scale"]
        origin_pose = scene_config["target_pose"]
        sample_pts = self.sample_pts * np.array([target_scale.tolist() + [1., ]])  # (N_t, 3)
        ic(target_scale, origin_pose, sample_pts.shape)

        angle = np.radians(-origin_pose[2])
        origin_pose[2] = 0
        ic(origin_pose, angle)
        translation_origin = origin_pose[np.newaxis, :, np.newaxis]
        local_traj = global_traj - translation_origin

        transform = np.zeros((sample_pts.shape[0], 2, 3))
        transform[:, :2, -1] = -sample_pts[:, :2]
        transform[:, 0, 0] = np.cos(angle)
        transform[:, 0, 1] = -np.sin(angle)
        transform[:, 1, 0] = np.sin(angle)
        transform[:, 1, 1] = np.cos(angle)

        print("==="*40)

        local_traj = np.einsum("ijk, lkm->ijlm", transform, local_traj)
        return local_traj

    def get_sample_point_traj(self, origin_traj_file_list, scales: np.ndarray) -> np.ndarray:
        """
        read the origin trajectories from files, transform it to the traj of all sample points in the shape (J, T, N, 2)

        J: number of sampled points, N: total number of demonstrations, T: total time steps
        """
        sample_pts_traj = []
        for file, scale in zip(origin_traj_file_list, scales):
            sample_pts = self.sample_pts * np.array([scale.tolist() + [1., ]])
            traj = np.load(file).squeeze()  # (T, 3)
            T = traj.shape[0]

            angle = np.radians(traj[:, 2])
            transform = np.zeros((T, 3, 3))
            transform[:, :2, -1] = traj[:, :2]
            transform[:, 0, 0] = np.cos(angle)
            transform[:, 0, 1] = -np.sin(angle)
            transform[:, 1, 0] = np.sin(angle)
            transform[:, 1, 1] = np.cos(angle)
            transform[:, 2, :] = np.array([[0, 0, 1]]*T)

            transformed_traj = np.einsum("ijk, lk->lij", transform, sample_pts)  # (T, 3, 3) (J, 3) -> (J, T, 3)
            sample_pts_traj.append(np.expand_dims(transformed_traj[:, :, :2], 2))
        return np.concatenate(sample_pts_traj, axis=2)

