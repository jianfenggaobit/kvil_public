import cv2
import copy
import click
import logging
import datetime
import numpy as np
from random import uniform
from os.path import join
from icecream import ic
from scipy import interpolate
from vil.utils.py.filesystem import create_path
from vil.utils.py.utils import save_to_yaml, load_dict_from_yaml
from vil.utils import get_data_path
from vil.toy.tools import Tool


class MainWindowAPP(object):
    def __init__(self, target, tool, info: str = "", path: str = "", width: int = 1200, height: int = 600):
        if path:
            self.path_root = path
            self._config_dict = load_dict_from_yaml(join(path, "scene_config.yaml"))
            if target != self._config_dict["obj"][0] or tool != self._config_dict["obj"][1]:
                logging.error(f"You are using different object than the ones used in your path "
                              f"{self._config_dict['obj']} vs yours [{target}, {tool}], please check {path}")
                exit()
            self._trial = len(self._config_dict['scale'])
            logging.info(f"this path already includes {self._trial} recordings.")
            self._trial += 1
        else:
            self.path_root = create_path(join(
                get_data_path(), "demo/toy",
                f"{target}_{tool}_{datetime.datetime.now():%Y-%m-%d-%H-%M-%S}")
            )
            self._config_dict = dict(description=info, obj=[target, tool], rool=["target", "tool"], scale=[])
            self._trial = 1

        self._config_file_name = join(self.path_root, "scene_config.yaml")
        self.target = Tool(target, role="Target")
        self.tool = Tool(tool, role="Tool")
        self.height, self.width = height, width
        self._pause = False

        # display window
        self._window_name = 'Shape configuration'
        self.background = np.zeros((self.height, self.width, 3), dtype=np.uint8) + 255
        self._scene_created = False
        self._random_scene()

        # init traj generation and log
        self.via_pose_tool = []
        self.trajectory = np.empty(0)

        # control bars
        self._create_trackbar()
        self._paint_window()
        self._scene_created = True

    def _random_scene(self):
        # global pos x-y plane form
        self.init_pose_target = np.array([
            uniform(200, 600), uniform(100, 300), uniform(-30, 30)
        ])
        self.init_pose_tool = np.array([
            uniform(600, 1000), uniform(100, 500), uniform(-40, 40)
        ])
        if self._scene_created:
            cv2.setTrackbarPos('target_r', self._window_name, int(self.init_pose_target[2]) + 180)
        self.rotation_degree = self.init_pose_tool[2]
        self.translation = self.init_pose_tool[:2]
        self.target_scale = (1, 1, self.init_pose_target[2])
        self.tool_scale = (1, 1)

    def _create_trackbar(self):
        cv2.namedWindow(self._window_name)
        cv2.moveWindow(self._window_name, (1920-self.width)//2, 10)

        def set_target(x):
            self._refresh_target()

        def set_tool(x):
            self._refresh_tool()

        def set_rotate_speed(x):
            self._refresh_rotate_speed()

        cv2.createTrackbar('target_w', self._window_name, 100, 1000, set_target)
        cv2.createTrackbar('target_h', self._window_name, 100, 1000, set_target)
        cv2.createTrackbar('target_r', self._window_name, int(self.init_pose_target[2])+180, 360, set_target)
        cv2.createTrackbar('tool_w', self._window_name, 100, 1000, set_tool)
        cv2.createTrackbar('tool_h', self._window_name, 100, 1000, set_tool)
        cv2.createTrackbar('rot_speed', self._window_name, 5, 30, set_rotate_speed)

    def _get_target_scale(self):
        target_w = cv2.getTrackbarPos('target_w', self._window_name) / 100
        target_h = cv2.getTrackbarPos('target_h', self._window_name) / 100
        target_r = cv2.getTrackbarPos('target_r', self._window_name)
        return target_w, target_h, target_r
    
    def _get_tool_scale(self):
        tool_w = cv2.getTrackbarPos('tool_w', self._window_name) / 100
        tool_h = cv2.getTrackbarPos('tool_h', self._window_name) / 100
        return tool_w, tool_h

    def _get_rot_speed_scale(self):
        return cv2.getTrackbarPos('rot_speed', self._window_name)

    def _paint_window(self):
        self._refresh_target()
        self._refresh_tool()
        self._refresh_rotate_speed()

    def _refresh_target(self):
        self.target_scale = self._get_target_scale()
        self.target.scale(np.array(self.target_scale[:2]))
        self.canvas_env = copy.deepcopy(self.background)
        self.pen_target_xy, self.pen_target = self.target.update(self.canvas_env, self.target_scale[2]-180, self.init_pose_target[:2])
        self.target.draw(self.canvas_env, self.pen_target)
        cv2.imshow(self._window_name, self.canvas_env)

    def _refresh_tool(self):
        self.tool_scale = self._get_tool_scale()
        self.tool.scale(np.array(self.tool_scale))
        canvas_tool = copy.deepcopy(self.canvas_env)
        self.pen_tool_xy, self.pen_tool = self.tool.update(canvas_tool, self.rotation_degree, self.translation)
        self.tool.draw(canvas_tool, self.pen_tool)
        cv2.imshow(self._window_name, canvas_tool)

    def _refresh_rotate_speed(self):
        self.rotate_speed = self._get_rot_speed_scale()

    def _get_mouse_pos(self, event, u, v, flags, param):
        self.mouse_xy = np.array([u, self.height - v]).astype(int)

        if not self._pause:
            self.translation = self.mouse_xy
            self._refresh_tool()

        if event == cv2.EVENT_LBUTTONDOWN:
            logging.info(f"add ({u}, {v}, {self.rotation_degree}) as via pose")
            self.via_pose_tool.append([self.mouse_xy[0], self.mouse_xy[1], self.rotation_degree])

    def generate(self):
        n_sample = len(self.via_pose_tool)
        ic(n_sample, self.via_pose_tool)
        if n_sample == 0:
            logging.warning("Nothing to generate")
        x = np.linspace(0, 1, n_sample)
        xnew = np.linspace(0, 1, 500)

        def inter_result(x, y, xnew):
            tck = interpolate.splrep(x, y, s=0, k=2)
            return interpolate.splev(xnew, tck, der=0)

        sample_y = np.array(self.via_pose_tool)
        ic(sample_y)
        u = inter_result(x, sample_y[:, 0], xnew)
        v = inter_result(x, sample_y[:, 1], xnew)
        r = inter_result(x, sample_y[:, 2], xnew)
        traj = np.vstack([u, v, r]).T
        self.trajectory = np.vstack([u, v, r]).T
        self.simulate(traj)

    def simulate(self, traj):
        logging.info("simulate the generated trajectory")
        self._pause = True
        for i in range(traj.shape[0]):
            print(f"{i}\r", end="")
            pose = traj[i, :]
            self.translation = pose[:2]
            self.rotation_degree = pose[2]
            self._refresh_tool()
            cv2.waitKey(10)
        self._pause = False

    def _save_trajectory(self):
        """ save the tool trajectory in (u, v) """
        traj = self.trajectory
        ic(traj.shape)
        traj = traj.reshape((-1, 500, 3))
        file_tool_traj = join(self.path_root, f'traj_{self.tool.name}_{self._trial:>04d}.npy')
        np.save(file_tool_traj, traj)

        target_pose = np.array([self.init_pose_target[0], self.init_pose_target[1], self.target_scale[2]-180])
        file_target_traj = join(self.path_root, f'traj_{self.target.name}_{self._trial:>04d}.npy')
        np.save(file_target_traj, np.tile(target_pose, (500, 1)))

        # save config info: object names, scaling vectors, etc
        target_scale = np.array(self.target_scale[:2])
        tool_scale = np.array(self.tool_scale)

        self._config_dict["scale"].append([target_scale.tolist(), tool_scale.tolist()])
        save_to_yaml(self._config_dict, filename=self._config_file_name)

        logging.info(f"saving trajectory to {file_tool_traj}")
        self._trial += 1

    def _reset_variables(self):
        self.rotation_degree = self.init_pose_tool[2]
        self._refresh_target()
        self._refresh_tool()
        self.via_pose_tool.clear()
        self.trajectory = np.empty(0)

    def run(self):
        cv2.setMouseCallback(self._window_name, self._get_mouse_pos)

        while True:
            k = cv2.waitKey(10) & 0xFF

            if k == 27:
                break
            elif k == ord('g'):
                self.generate()

            elif k == ord('r'):
                self._random_scene()
                self._paint_window()

            elif k == ord('q'):
                logging.info('Give up current recording')
                self._reset_variables()

            # rotation control
            elif k == ord('a'):
                logging.info('Rotate left')
                self.rotation_degree += self.rotate_speed
                self._refresh_tool()
            elif k == ord('d'):
                logging.info('Rotate right')
                self.rotation_degree -= self.rotate_speed
                self._refresh_tool()

            elif k == ord('l'):
                self._save_trajectory()


@click.command(context_settings=dict(help_option_names=['-h', '--help']))
@click.option("--target",  "-ta",   type=str,   default="TARGET",   help="the name of the target")
@click.option("--tool",    "-to",   type=str,   default="TOOL",     help="the name of the tool")
@click.option("--info",    "-i",    type=str,   default="",         help="additional user information to keep")
@click.option("--path",    "-p",    type=str,   default="",         help="if user specified path, use it as logging"
                                                                         "path, if exists, append to existing data")
def main(target, tool, info, path):
    try:
        app = MainWindowAPP(target, tool, info, path)
        app.run()
    finally:
        cv2.destroyAllWindows()


if __name__ == '__main__':
    main()
