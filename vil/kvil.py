import os
import copy
import logging
import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
from matplotlib.widgets import Button

from marshmallow_dataclass import dataclass

from os.path import join
from matplotlib.widgets import Slider
from scipy.optimize import minimize
import scipy.stats as st
from typing import List, Tuple, Union
from pathos.multiprocessing import ProcessingPool as Pool
from functools import partial
from sklearn.decomposition import PCA
from sklearn.cluster import AgglomerativeClustering

from mpl_toolkits.mplot3d.art3d import Line3DCollection, Poly3DCollection

from vil.utils.math.transformations import euler_matrix
from vil.utils.py.filesystem import get_ordered_files, create_path
from vil.utils.py.utils import load_dict_from_yaml, load_dataclass_from_yaml, dump_data_to_yaml, load_dataclass, save_to_yaml
from vil.utils.py.visualize import draw_frame_2d, draw_frame_3d, set_3d_equal_auto, set_2d_equal_auto

from vil.vmp import VMP
from vil.toy.tools.tools import Tool
from vil.real_object import RealObject
from vil.pme import pme, mapping, hdmde, projection_index


def load_npz_to_dict(filename):
    temp = np.load(filename)
    return {key: temp[key] for key in temp}


def get_rot_mat_2d(alpha: float):
    return np.array([[np.cos(alpha), -np.sin(alpha)], [np.sin(alpha), np.cos(alpha)]])


def get_rot_mat_3d(euler: np.ndarray):
    return euler_matrix(*euler.tolist(), axes="sxyz")[:3, :3]


def get_loss(
        dim: int,
        origin: np.ndarray,
        orientation_config: np.ndarray,
        new_global_coords: np.ndarray,
        original_local_coords: np.ndarray
) -> float:
    """
    Compute the MSE loss given a frame configuration, current global coordinates and the expected local coordinates
    """
    if dim == 3:
        rot_mat = get_rot_mat_3d(orientation_config[dim:])
    else:
        rot_mat = get_rot_mat_2d(orientation_config[2])
    local_coords = np.einsum("ij, kj -> ki", rot_mat.T, (new_global_coords - orientation_config[:dim]))
    loss = np.linalg.norm((original_local_coords - local_coords), axis=-1).mean()
    return loss


def find_local_frame(
        new_point_coordinates: np.ndarray,
        init_config: np.ndarray,
        init_point_coordinates: np.ndarray,
        n_neighbor_pts_for_origin: int,
        dim: int = 2,
        idx_of_detected: np.ndarray = None,
        return_matrix: bool = False
) -> np.ndarray:
    """
    Given detected points in the new scene, optimize a local frame so that when these new points are projected into such
    frame, the MSE between the result and the init_point_coordinates is minimal.

    Args:
        dim: task space dimension
        new_point_coordinates: (K, dim)
        init_config: for 3d, its position + quaternion [x, y, z, roll, pitch, yaw], for 2d its [x, y, alpha]
        init_point_coordinates: (K, dim)
        n_neighbor_pts_for_origin: how many number of neighbor to consider to estimate origin
        idx_of_detected: in case of some point not been detected, you can specify the index of detected ones, this also
            means that the new_point_coordinates only contains coordinates of the detected points.
        return_matrix: whether to return the configuration as a transposed tranformation matrix

    Returns:
        new frame configuration, of same format as init_config

    """
    if idx_of_detected is not None:
        init_point_coordinates = init_point_coordinates[idx_of_detected]

    # use the first n_origin_neighbor_pts to estimate the origin of the local frame, the optimization only takes
    # care of matching the orientation
    origin = np.median(new_point_coordinates[:n_neighbor_pts_for_origin], axis=0)

    def obj(orientation_config):
        return get_loss(dim, origin, orientation_config, new_point_coordinates, init_point_coordinates)

    initial_config = copy.deepcopy(init_config)
    initial_config[:dim] = copy.deepcopy(origin)
    res = minimize(obj, initial_config, method='SLSQP', tol=1e-6)
    final = res.x
    final[:dim] = origin
    if return_matrix:
        if dim == 2:
            rot_mat_T = get_rot_mat_2d(final[-1]).T
        elif dim == 3:
            rot_mat_T = get_rot_mat_3d(final[dim:]).T
        else:
            raise NotImplementedError
        return np.concatenate((rot_mat_T, rot_mat_T.dot(origin).reshape(-1, 1)), axis=-1)  # 2d: (2, 3), 3d: (3, 4)
    else:
        return final


def init_frame_config(
        dim: int,
        point_index: np.ndarray,
        sample_points_coordinates: np.ndarray = None,
        num_neighbor_pts: int = None
) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    """
    Get the initial status of the local frame and sample some reference points around its origin, which will be used in
    later optimization step to find this local frame again in the new scene.

    Args:
        dim: the dimension of the task space (2, or 3)
        point_index: the descriptor of the point (pixel), toy prob: the point index as ndarray
        # obj_info: dictionary including object information, e.g. 2d toy example, it includes a Tool object
        sample_points_coordinates: the coordinates of all the sampled points on the object
        num_neighbor_pts: number of neighboring points to be considered

    Returns:
        init_frame_config: the initial local frame w.r.t. to global frame. 2D: [x, y, alpha], 3D: [x, y, z, qw, qx, qy, qz]
        init_point_coordinates: sampled point coordinates in the init_frame array(K, dim)
        descriptor: descriptor of the sampled points. 2D: neighboring index, 3D: DCN descriptor array(K, dim(

    """

    # take random frames from demo as ref image, for toy problem, just use the primitive shape of the object
    this_point_coord = sample_points_coordinates[point_index].flatten()
    dist_array = np.linalg.norm((sample_points_coordinates - this_point_coord), axis=-1)
    neighbor_idx = np.argsort(dist_array)[:num_neighbor_pts]
    if dim == 2:
        init_frame_config_array = np.array([this_point_coord[0], this_point_coord[1], 0.0])
    else:
        init_frame_config_array = np.array(this_point_coord.tolist() + [0.0, 0.0, 0.0])

    init_point_coordinates = sample_points_coordinates[neighbor_idx] - this_point_coord
    return init_frame_config_array, init_point_coordinates, neighbor_idx


def proj_to_local(transform: np.ndarray, traj: np.ndarray, dim=2):
    """
    project trajectories of all (sum J\i) points in all N demonstrations to one specific local frame

    Args:
        transform: (T, N, 2, 3) or (T, N, 3, 4)
        traj: (sum J\i, T, N, 2) or (sum J\i, T, N, 3)
        dim: 2 or 3

    Returns: local traj of shape (T, sum J\i, N, dim)

    """
    #          i, j, k,   l       m,       i, j, l    -> i   m        j  k
    # einsum: (T, N, dim, dim+1) (sum J\i, T, N, dim) -> (T, sum J\i, N, dim)
    # sub:    (T, sum J\i, N, dim) - (T, 1, N, dim) -> (T, sum J\i, N, dim)
    return np.einsum("ijkl, mijl->imjk", transform[..., :dim], traj) - np.expand_dims(transform[..., -1], 1)


def pca(data_points: np.ndarray):
    """
    Principal Component Analysis
    Args:
        data_points: (N, dim)

    Returns: concatenated array of components, mean and explained variance
    """
    pca = PCA().fit(data_points)
    if len(pca.mean_) == 3 and len(pca.explained_variance_) == 2:
        explained_variance = np.concatenate((pca.explained_variance_, np.array([0.0])))
    else:
        explained_variance = pca.explained_variance_
    return np.concatenate((pca.components_, np.expand_dims(pca.mean_, 0), np.expand_dims(explained_variance, 0)),
                   axis=0)


def pme_func(data_points: np.ndarray, intrinsic_dim: int = 1):
    """
    principal manifold estimation
    Args:
        data_points: (N, dim)
        intrinsic_dim: the dimension of the expected principal manifold

    Returns:

    """
    logging.info("---" * 10 + " run pme " + "---" * 10)
    max_value = np.abs(data_points).max() * 0.5
    data_points /= max_value
    sol_opt_x, sol_opt_t, t_opt, label, embedding, projection = pme(
        data_points, d=intrinsic_dim, tuning_para_seq=np.exp(np.arange(-5, 6)), max_comp=data_points.shape[0]-1
    )
    stress_vectors = data_points - projection
    std_stress = np.linalg.norm(stress_vectors, axis=-1).std()
    std_projection = np.linalg.norm(embedding.var(axis=0))

    w, mu, sig = vis_pme(data_points, sol_opt_x, sol_opt_t, t_opt, label, embedding, projection, intrinsic_dim, True)
    return sol_opt_x, sol_opt_t, t_opt, std_stress, std_projection, embedding, max_value, w, mu, sig, projection


def vis_pme(data_points, sol_opt_x, sol_opt_t, t_opt, label, embedding, projection, d, plot=False):
    # Note: mapping (intrinsic -> observation space)
    t_test = np.arange(-3, 3, 0.05)
    x_test = mapping(t_test, d=d, sol_opt_x=sol_opt_x, sol_opt_t=sol_opt_t, t_opt=t_opt)

    # Note: optimization based projection index (observation space -> intrinsic)
    mean_data_point = mapping(embedding.mean(keepdims=True), d=d, sol_opt_x=sol_opt_x,
                              sol_opt_t=sol_opt_t, t_opt=t_opt)
    idx_t_larger_than_mean = np.where(t_test > embedding.mean())[0]
    idx_t_smaller_than_mean = np.where(t_test < embedding.mean())[0]

    # Note: compute density on manifold
    density_data = embedding.reshape((-1, d))
    theta_hat, mu, sig, label = hdmde(density_data, n_cluster_min=1, alpha=0.1, max_comp=1)

    if d == 1:
        t_test.reshape(-1, 1)
        p = np.zeros_like(t_test)
        prob_derivative = np.zeros_like(t_test)
        for i in range(mu.shape[0]):
            prob_ = st.multivariate_normal.pdf(t_test.reshape(-1, 1), mu[i], sig) * theta_hat[i]
            p += prob_
            prob_derivative -= prob_ * (t_test - mu[i]) / (sig ** 2)
    elif d == 2:
        lim = 4
        x, y = np.mgrid[-lim:lim:31j, -lim:lim:31j]
        test_data = np.stack((x.ravel(), y.ravel())).T
        p = np.zeros(test_data.shape[0])
        prob_derivative = np.zeros_like(test_data)
        for i in range(mu.shape[0]):
            prob_ = st.multivariate_normal.pdf(test_data, mu[i], sig) * theta_hat[i]
            p += prob_
            prob_derivative -= prob_.reshape(-1, 1) * (test_data - mu[i]) / (sig ** 2)
    else:
        raise ValueError
    entropy = - (p * np.log(p)).sum()

    if plot:
        # Note: density fucntion as polygon
        scaling = 5
        density_func = x_test.copy()
        density_func[:, 2] += p * scaling
        density_polygon_larger_than_mean = np.concatenate(
            (density_func[idx_t_larger_than_mean], x_test[idx_t_larger_than_mean][::-1]),
            axis=0)
        density_polygon_smaller_than_mean = np.concatenate(
            (density_func[idx_t_smaller_than_mean], x_test[idx_t_smaller_than_mean][::-1]),
            axis=0)

        # Note plot
        fig = plt.figure(figsize=(16, 9))
        spec = fig.add_gridspec(ncols=1, nrows=1)
        ax = fig.add_subplot(spec[0, 0], projection="3d")

        # Note plot stress vector
        stress_vectors = np.concatenate((data_points, projection), axis=-1).reshape(
            (-1, 2, 3))
        lc = Line3DCollection(stress_vectors, colors="k", linewidths=2, zorder=-1)
        ax.add_collection(lc)

        # Note plot density polygon
        poly3d_larger_than_mean = [density_polygon_larger_than_mean.tolist()]
        poly3d_smaller_than_mean = [density_polygon_smaller_than_mean.tolist()]
        ax.add_collection3d(
            Poly3DCollection(poly3d_larger_than_mean, facecolors='paleturquoise', linewidths=2,
                             alpha=0.5, edgecolors="turquoise", zorder=-1))
        ax.add_collection3d(
            Poly3DCollection(poly3d_smaller_than_mean, facecolors='paleturquoise', linewidths=2,
                             alpha=0.5, edgecolors="turquoise", zorder=-1))

        # Note: scatter plots, data points, projection, mean
        ax.scatter(data_points[:, 0], data_points[:, 1], data_points[:, 2], color="k", s=30)
        ax.scatter(projection[:, 0], projection[:, 1],
                   projection[:, 2], color="limegreen", alpha=0.8, s=150, zorder=1)
        ax.scatter(mean_data_point[:, 0], mean_data_point[:, 1], mean_data_point[:, 2],
                   color="yellow", edgecolor='k', s=850, zorder=10)

        # Note: plot principal curve
        ax.plot(x_test[:, 0], x_test[:, 1], x_test[:, 2], color="darkslategrey", zorder=5)

        # Note: set axis
        set_3d_equal_auto(ax)
        ax.set_axis_off()
        plt.show()
    return theta_hat, mu, sig


@dataclass
class Constraints:
    idx:                            int = 0
    type:                           str = "p2p"
    priority:                       int = 0
    target_obj:                     str = ""
    local_frame_idx:                int = None
    local_frame_config_array:       list = None
    local_frame_config_coordinates: list = None
    local_frame_config_descriptors: list = None
    salient_obj:                    str = ""
    salient_obj_spatial_scale:      float = 1.0
    kpts_index:                     int = None
    kpts_descriptor:                list = None
    time_step:                      int = None

    pca_components:                 list = None
    pca_mean:                       list = None
    pca_var:                        list = None

    pm_sol_opt_x:                   list = None
    pm_sol_opt_t:                   list = None
    pm_t_opt:                       list = None
    pm_embedding:                   list = None
    pm_std_stress:                  float = 0.0
    pm_std_projection:              float = 0.0
    pm_intrinsic_dim:               int = 0
    pm_scaling:                     float = 1.0

    pm_imit_kpts_position_local:    list = None
    pm_init_proj_point:             list = None
    pm_init_tangent_vec:            list = None
    pm_init_proj_value:             float = 0.0

    # the following are for deployment
    vmp_kernel:                     int = 20
    vmp_dim:                        int = 3
    vmp_weights:                    list = None
    vmp_traj_files:                 list = None
    vmp_goal:                       list = None

    kpts_obs_local:                 list = None
    vmp_start:                      list = None
    local_frame_rot_mat_obs:        list = None
    local_frame_origin_obs:         list = None

    density_mu:                     list = None
    density_weights:                list = None
    density_std_dev:                float = 0.0

    def __repr__(self):
        return f"Constraint {self.idx+1:>2d} type {self.type:>4}, " \
               f"{self.local_frame_idx+1:>3d}-th local frame defined on {self.target_obj:>10}, " \
               f"  with {self.kpts_index+1:>3d}-th keypoints on {self.salient_obj:>10}," \
               f"  restricted at time step {self.time_step+1:>3d}."


@dataclass
class KVILConfig:
    dim:                        int = 3
    n_cpu:                      int = os.cpu_count()
    n_neighbor_pts_for_origin:  int = 5
    n_neighbor_pts:             int = 50  # > n_origin_neighbor_pts
    n_sample_pts_max:           int = 2000
    traj_downsample_size:       int = 3
    kpts_downsample_size:       int = 300

    th_lv:                      float = 0.02
    th_linear_lv:               float = 0.01
    th_linear_hv:               float = 0.3
    principal_var_ratio:        float = 4.0
    th_time_cluster:            int = 5
    th_dist_ratio:              float = 0.3

    last_time_step_pce:         bool = True
    remove_outlier:             bool = True


class KVIL:
    def __init__(self, record_path: str, force_redo: bool = False, viz: bool = True, viz_debug: bool = False, time_steps: int = 0):
        """
        Given the path, read scene configuration, config the analyzer properly according to dimension (dim) of the task

        Args:
            record_path: path to the recorded demonstrations
            dim: either 2 for toy problem or 3 for real world application
            force_redo: force the algorithm to recompute everything
            viz: allow visualization of the final results
            viz_debug: allow visualization of intermediate results
            time_steps: set how many times steps of the trajectory to visualize
        """
        self.viz = viz
        self.viz_debug = viz_debug
        self.force_redo = force_redo
        self.path = record_path
        self.proc_path = create_path(join(self.path, "process"))
        self.c = load_dataclass_from_yaml(KVILConfig, join(record_path, "_kvil_config.yaml"))
        self.set_vis_time_steps(time_steps)

        scene_config_dict = load_dict_from_yaml(join(record_path, "_scene_config.yaml"))
        traj_path = join(record_path, "raw_traj")
        self.obj_list = scene_config_dict["obj"]
        self._n_obj = len(self.obj_list)
        self.traj_file_list = [
            get_ordered_files(traj_path, pattern=[f"traj_{obj_name}"], depth="/*")
            for obj_name in self.obj_list
        ]
        self.n_demo = len(self.traj_file_list[0])

        # construct objects in the scene
        if self.c.dim == 2:
            self._objects = [Tool(name=obj_name) for obj_name in self.obj_list]  # type: List[Tool]
            self._scale = np.array(scene_config_dict["scale"]).reshape((self.n_demo, self._n_obj, 2))

        elif self.c.dim == 3:
            self._objects = [
                RealObject(
                    name=obj_name, path=record_path, force_redo=force_redo, remove_outlier=self.c.remove_outlier,
                    last_time_step_pce=self.c.last_time_step_pce
                )
                for obj_name in self.obj_list
            ]

        # Note: prepare intermediate data
        self.var = np.empty(0)

        self._p_low_var = []
        self._p_pcurv = []
        self._p_psurf = []

        self.master_idx = 0

        self.constraints = []  # type: List[Constraints]
        self.constraints_count = np.zeros((self._n_obj, self._n_obj), dtype=int)
        self.mask = []  # for each object, record the masks of handled data points, to exclude them for next stages
        self.total_time_steps = 0

        self.constraints_index_lists = [[] for _ in range(self._n_obj**2)]  # type: List[List[int]]

        self.t_c, self.p_c, self.d_c = None, None, None
        self.theta_c, self.theta_d, self.theta_p = {}, {}, {}  # for each object: (J_i, 3), (J_i, M, 1), (J_i, M, 2)
        self.local_traj_dict, self.frame_mat_dict = {}, {}
        self.pca_res_dict, self.pme_res_dict = {}, {}
        self.pme_idx = []
        self.obj_variations = np.ones(self._n_obj, dtype=bool)

    def set_vis_time_steps(self, time_steps):
        prev_timestep_file = join(self.proc_path, "time_steps.yaml")
        if os.path.isfile(prev_timestep_file):
            prev_timesteps = load_dict_from_yaml(prev_timestep_file)["time_steps"]
        else:
            prev_timesteps = -1
        if time_steps == 0:
            if prev_timesteps == -1:
                time_steps = 2
                self.force_redo = True
            else:
                time_steps = prev_timesteps
                self.force_redo = False
        else:
            if time_steps == prev_timesteps:
                self.force_redo = False
            else:
                self.force_redo = True
        time_steps = max(min(time_steps, 20), 2)
        self.c.traj_downsample_size = time_steps
        save_to_yaml(dict(time_steps=time_steps), prev_timestep_file)

    def initialize(self):
        """
        step 0: initialize (* means only required for real world data)
        - get a list of object
        - * find intersection region: visible points in all demonstration (especially useful in real-world applications)
        - * sample J points
        - construct index array of all candidate points. p_c (I, J)
        - construct descriptor array of them. d_c (I, J, 1) or (I, J, 3)
        - load their trajectories in global frame. t_c (I, J, N, T, 2) or (I, J, N, T, 3)
        - compute init frame config, save intermediate results
        """
        self.log(" initialize ")
        preproc_file = join(self.proc_path, "preproc.npz")
        if os.path.isfile(preproc_file) and not self.force_redo:
            logging.info(f"loading from {preproc_file}")
            data = np.load(preproc_file, allow_pickle=True)
            self.p_c, self.d_c, self.t_c = data["p_c"], data["d_c"], data['t_c']
            self.theta_c = load_npz_to_dict(join(self.proc_path, "theta_c.npz"))
            self.theta_d = load_npz_to_dict(join(self.proc_path, "theta_d.npz"))
            self.theta_p = load_npz_to_dict(join(self.proc_path, "theta_p.npz"))
            if self.viz_debug and False:
                for i in range(self._n_obj):
                    obj_name = self.obj_list[i]
                    idx_start_end = data["p_c"][i]
                    idx = np.arange(idx_start_end[0], idx_start_end[1])
                    if self.c.dim == 2:
                        self.viz_frame_matching_2d(self.obj_list[i], self.theta_c[obj_name], self.theta_d[obj_name], self.theta_p[obj_name])
                    else:
                        self.viz_frame_matching_3d(self._objects[i], self.theta_c[obj_name], self.theta_d[obj_name], self.theta_p[obj_name])
        else:
            p_c, d_c, t_c = [], [], []
            if self.c.dim == 2:
                previous_idx = 0
                for i, (obj_name, obj, traj_files) in enumerate(zip(self.obj_list, self._objects, self.traj_file_list)):
                    traj_i = obj.get_sample_point_traj(traj_files, self._scale[:, i])
                    down_sample_idx = np.linspace(0, traj_i.shape[1]-1, self.c.traj_downsample_size).astype(int)
                    traj_i = traj_i[:, down_sample_idx]
                    t_c.append(traj_i)  # (J, T, N, 2)

                    sample_points_coordinates = obj.get_scaled_sample_point_coordinates(self.c.n_sample_pts_max)
                    n_sample_pts = sample_points_coordinates.shape[0]
                    p_c.append([previous_idx, previous_idx+n_sample_pts])
                    d_c.append(np.arange(n_sample_pts))
                    previous_idx += n_sample_pts

                    init_frame_config_array, init_point_coordinates, descriptor = [], [], []
                    for idx in range(n_sample_pts):
                        cfg_array, coordinates, des = init_frame_config(
                            dim=self.c.dim, point_index=np.array([idx]),
                            sample_points_coordinates=sample_points_coordinates,
                            num_neighbor_pts=self.c.n_neighbor_pts
                        )
                        init_frame_config_array.append(cfg_array)
                        init_point_coordinates.append(coordinates)
                        descriptor.append(des)

                    self.theta_c[obj_name] = np.vstack(init_frame_config_array).reshape((n_sample_pts, 3))  # (J, 3)
                    self.theta_d[obj_name] = np.vstack(descriptor).reshape((n_sample_pts, self.c.n_neighbor_pts, 1))   # (J, M, 1)
                    self.theta_p[obj_name] = np.vstack(init_point_coordinates).reshape((n_sample_pts, self.c.n_neighbor_pts, 2))  # (J, M, 2)

                    if self.viz_debug:
                        self.viz_frame_matching_2d(obj_name, self.theta_c[obj_name], self.theta_d[obj_name], self.theta_p[obj_name])

                p_c = np.array(p_c)  # (I, 2) [start, end]
                d_c = np.concatenate(d_c, axis=0)  # (IJ,)
                t_c = np.concatenate(t_c, axis=0)  # (IJ, T, N, 2)

                np.savez(file=preproc_file,
                         p_c=p_c, d_c=d_c, t_c=t_c)
                np.savez(file=join(self.proc_path, "theta_c.npz"), **self.theta_c)
                np.savez(file=join(self.proc_path, "theta_d.npz"), **self.theta_d)
                np.savez(file=join(self.proc_path, "theta_p.npz"), **self.theta_p)

            elif self.c.dim == 3:
                previous_idx = 0
                for i, (obj_name, obj, traj_files) in enumerate(zip(self.obj_list, self._objects, self.traj_file_list)):

                    traj_i = obj.get_sample_point_traj(
                        traj_files, remove_outlier=self.c.remove_outlier,
                        n_samples=self.c.kpts_downsample_size, viz_debug=self.viz_debug)
                    down_sample_idx = np.linspace(0, traj_i.shape[1] - 1, self.c.traj_downsample_size).astype(int)
                    traj_i = traj_i[:, down_sample_idx]
                    t_c.append(traj_i)  # (J, T, N, 3)

                    sample_points_coordinates = obj.get_scaled_sample_point_coordinates(self.c.n_sample_pts_max)
                    n_sample_pts = sample_points_coordinates.shape[0]

                    p_c.append([previous_idx, previous_idx + n_sample_pts])
                    d_c.append(np.arange(n_sample_pts))
                    previous_idx += n_sample_pts

                    init_frame_config_array, init_point_coordinates, descriptor = [], [], []
                    num_neighbor_pts = 10 if "hand" in obj_name else self.c.n_neighbor_pts
                    for idx in range(n_sample_pts):
                        cfg_array, coordinates, des = init_frame_config(
                            dim=self.c.dim, point_index=np.array([idx]),
                            sample_points_coordinates=sample_points_coordinates, num_neighbor_pts=num_neighbor_pts)
                        init_frame_config_array.append(cfg_array)
                        init_point_coordinates.append(coordinates)
                        descriptor.append(des)

                    self.theta_c[obj_name] = np.vstack(init_frame_config_array).reshape((n_sample_pts, 6))  # (J, 6)
                    self.theta_d[obj_name] = np.vstack(descriptor).reshape((n_sample_pts, num_neighbor_pts, 1))  # (J, M, 1)
                    self.theta_p[obj_name] = np.vstack(init_point_coordinates).reshape((n_sample_pts, num_neighbor_pts, 3))  # (J, M, 3)

                    if self.viz_debug:
                        self.viz_frame_matching_3d(obj, self.theta_c[obj_name], self.theta_d[obj_name], self.theta_p[obj_name],
                                              real_traj=traj_i)

                p_c = np.array(p_c)  # (I, 2) [start, end]
                d_c = np.concatenate(d_c, axis=0)  # (IJ,)
                t_c = np.concatenate(t_c, axis=0)  # (IJ, T, N, 2)

                np.savez(
                    file=preproc_file,
                    p_c=p_c, d_c=d_c, t_c=t_c,
                    # **dict(theta_c=self.theta_c, theta_d=self.theta_d, theta_p=self.theta_p)
                )
                np.savez(file=join(self.proc_path, "theta_c.npz"), **self.theta_c)
                np.savez(file=join(self.proc_path, "theta_d.npz"), **self.theta_d)
                np.savez(file=join(self.proc_path, "theta_p.npz"), **self.theta_p)
            else:
                raise ValueError("dim only takes value of 2 or 3")

            self.t_c = t_c
            self.p_c = p_c
            self.d_c = d_c

        self.motion_saliency()
        self.total_time_steps = self.t_c.shape[1]
        logging.info(f"index range array p_c: {self.p_c.shape}, index_array d_c: {self.d_c.shape}, trajectory array t_c: {self.t_c.shape}")

    def motion_saliency(self):
        total_var_list = []
        for i in range(self._n_obj):
            global_traj_i = self.t_c[np.arange(self.p_c[i, 0], self.p_c[i, 1])]  # (J_i, T, N, 2)
            delta_motion = global_traj_i[:, 1:] - global_traj_i[:, :-1]
            delta_motion = delta_motion.reshape(-1, self.t_c.shape[-1])
            total_var_list.append(np.linalg.norm(delta_motion, axis=-1).sum())
        self.master_idx = np.argmin(np.array(total_var_list))
        save_to_yaml(data=dict(master_idx=self.master_idx.item()), filename=join(self.proc_path, "master_slave.yaml"))
        logging.info(f"the master object is {self.obj_list[self.master_idx]}")

    def frame_matching_and_projection(self):
        self.log(" local frame matching and projection ")
        local_traj_file = join(self.proc_path, "local_traj.npz")
        frame_mat_file = join(self.proc_path, "frame_mat.npz")

        n_total_candidates = self.p_c[-1, -1]

        if os.path.isfile(local_traj_file) and not self.force_redo:
            logging.info(f"loading from {local_traj_file}")
            traj_data = np.load(local_traj_file)
            frame_data = np.load(frame_mat_file)
            for obj_name in self.obj_list:
                self.frame_mat_dict[obj_name] = frame_data[obj_name]
                self.local_traj_dict[obj_name] = traj_data[obj_name]
        else:
            for i, (idx_start_end, obj) in enumerate(zip(self.p_c, self._objects)):
                logging.info(
                    f"-- the {i}-th object {self.obj_list[i]}: sampled points with index range {idx_start_end} --")
                idx = np.arange(idx_start_end[0], idx_start_end[1])
                init_frame_cfg_array = self.theta_c[obj.name]  # (J, 3) for 2d and (J, 7) for 3d
                init_frame_descriptor = self.theta_d[obj.name]  # (J, M, 1)
                init_frame_coordinate = self.theta_p[obj.name]  # (J, M, dim)

                traj_other_kpts = self.t_c[np.concatenate((  # (sum J\i, T, N, dim)
                    np.arange(0, idx_start_end[0]),
                    np.arange(idx_start_end[1], n_total_candidates))
                )]

                new_frame_mat_in_all_demo = []
                for n in range(self.n_demo):
                    logging.info(f"-- -- the {n}-th demo")

                    # t_c: (IJ, T, N, dim)
                    sampled_pts = self.t_c[idx, :, n, :]  # (J_i, T, dim)
                    new_coordinates = sampled_pts[init_frame_descriptor.squeeze()]  # (J_i, M, T, dim)
                    J_i, M, T, dim = new_coordinates.shape
                    new_coordinates = new_coordinates.transpose(0, 2, 1, 3).reshape((J_i * T, M, dim))

                    pool = Pool(self.c.n_cpu)
                    new_frame_mat = np.array(pool.map(
                        partial(find_local_frame, dim=dim, return_matrix=True, n_neighbor_pts_for_origin=self.c.n_neighbor_pts_for_origin),
                        new_coordinates,  # (J_i*T, M, dim)
                        init_frame_cfg_array.repeat(T, axis=0),  # (J_i*T, 3)
                        np.tile(init_frame_coordinate, (T, 1)).reshape((J_i * T, M, dim))  # (J_i*T, M, dim)
                    )).reshape((J_i, T, dim, dim + 1))  # (J_i, T, dim, dim+1)
                    new_frame_mat_in_all_demo.append(np.expand_dims(new_frame_mat, axis=-3))  # (J_i, T, 1, dim, dim+1)

                new_frame_mat_in_all_demo = np.concatenate(new_frame_mat_in_all_demo,
                                                           axis=-3)  # (J_i, T, N, dim, dim+1)
                logging.info("-- projection --")
                pool = Pool(self.c.n_cpu)
                traj = np.array(pool.map(  # (J_i, T, sum J\i, N, dim)
                    partial(proj_to_local, dim=self.c.dim, traj=traj_other_kpts),
                    new_frame_mat_in_all_demo
                ))
                self.frame_mat_dict[obj.name] = new_frame_mat_in_all_demo
                self.local_traj_dict[obj.name] = traj

            np.savez(local_traj_file, **self.local_traj_dict)
            np.savez(frame_mat_file, **self.frame_mat_dict)

    def distance_criteria(self):
        IJ, T, N, dim = self.t_c.shape
        i = self.master_idx
        obj = self._objects[i]
        logging.info(f"==== observing from object {obj.name} ====")
        local_traj = self.local_traj_dict[obj.name][:, [-1]]  # (J_i, T, sum J\i, N, dim)
        self.mask[i][:, :-1] = True

        # Note the distance criteria should only consider the distance between the closest frame to all
        #  candidate on the motion-salient (slave) object
        distance = np.linalg.norm(local_traj, axis=-1)[..., 0]  # (J_i, T, sum J\i, 1)
        frame_idx_argmin = np.where(distance == distance.min())[0]
        distance_ = distance[frame_idx_argmin, ...]

        idx_min = np.where(distance < np.percentile(distance_, 10))  # use the 10% percentile as threshold
        self.mask[i][:, [-1]][idx_min] = True

        for idx in range(self._n_obj):
            if idx == i:
                continue
            self.log(" min distance criterion ")
            self.select_lv_candidate(idx_min, distance[idx_min], obj_idx=i, constraint_type="p2p", assign_T=True)
            if self.constraints_count[i, idx] > 1:
                n_redundant = self.constraints_count[i, idx] - 1
                del self.constraints[-n_redundant:]
                del self.constraints_index_lists[i * self._n_obj + idx][-n_redundant:]
                self.constraints_count[i, idx] -= n_redundant

            if self.constraints_count[i, idx] < dim:
                self.log(" max distance criterion ")

                low_dist_frame_idx_ = self.constraints[self.constraints_index_lists[i * self._n_obj + idx][0]].local_frame_idx
                idx_max_ = np.where(np.invert(self.mask[i]))
                ele_idx_ = np.where(idx_max_[0] == low_dist_frame_idx_)[0]
                frame_idx_ = idx_max_[0][ele_idx_]
                time_idx_ = np.zeros_like(frame_idx_)
                kpts_idx_ = idx_max_[2][ele_idx_]
                idx_max = (frame_idx_, time_idx_, kpts_idx_)
                distance_ = distance[idx_max]

                max_dist_idx_ = distance[idx_max].argmax()

                constraint = Constraints()
                constraint.idx = len(self.constraints)
                constraint.type = "p2p"
                constraint.target_obj = self.obj_list[i]
                constraint.salient_obj = self.obj_list[idx]
                self.constraints_count[i, idx] += 1
                self.constraints_index_lists[i * self._n_obj + idx].append(constraint.idx)
                constraint.local_frame_idx = frame_idx_[max_dist_idx_]
                constraint.time_step = self.total_time_steps - 1
                constraint.kpts_index = kpts_idx_[max_dist_idx_]
                self.constraints.append(constraint)
                logging.info(f"-- -- {constraint}")

                filter_out_ele_idx_ = np.where(distance_ > np.percentile(distance_, 80))[0]
                frame_idx_ = frame_idx_[filter_out_ele_idx_]
                kpts_idx_ = kpts_idx_[filter_out_ele_idx_]
                self.mask[i][:, -1][(frame_idx_, kpts_idx_)] = True

                if self.constraints_count[i, idx] < dim:
                    self.log(" random sample ")
                    idx_max_ = np.where(np.invert(self.mask[i]))
                    ele_idx_ = np.where(idx_max_[0] == low_dist_frame_idx_)[0]
                    kpts_idx_ = idx_max_[2][ele_idx_]

                    kpt_idx_min = self.constraints[self.constraints_index_lists[i * self._n_obj + idx][0]].kpts_index
                    kpt_idx_max = self.constraints[self.constraints_index_lists[i * self._n_obj + idx][1]].kpts_index

                    candidate_coords_on_primitive_shape = self._objects[idx].get_sample_point_coordinates()[kpts_idx_]
                    dist = np.linalg.norm(
                        candidate_coords_on_primitive_shape -
                        self._objects[idx].get_sample_point_coordinates()[kpt_idx_max],
                        axis=-1
                    ) - np.linalg.norm(
                        candidate_coords_on_primitive_shape -
                        self._objects[idx].get_sample_point_coordinates()[kpt_idx_min],
                        axis=-1
                    )
                    ele_idx__ = np.abs(dist).argmin()
                    selected_kpt_index_ = kpts_idx_[ele_idx__]

                    constraint = Constraints()
                    constraint.idx = len(self.constraints)
                    constraint.type = "p2p"
                    constraint.target_obj = self.obj_list[i]
                    constraint.salient_obj = self.obj_list[idx]
                    self.constraints_count[i, idx] += 1
                    self.constraints_index_lists[i * self._n_obj + idx].append(constraint.idx)
                    constraint.local_frame_idx = low_dist_frame_idx_
                    constraint.time_step = self.total_time_steps - 1
                    constraint.kpts_index = selected_kpt_index_
                    self.constraints.append(constraint)
                    logging.info(f"-- -- {constraint}")

    def linear_constraints(self):
        self.log(" PCA ")

        pca_res_file = join(self.proc_path, "pca_result.npz")
        if os.path.isfile(pca_res_file) and not self.force_redo:
            logging.info(f"load PCA results from {pca_res_file}")
            self.pca_res_dict = np.load(pca_res_file)
        else:
            # computing variance using PCA
            for i, (idx_start_end, obj) in enumerate(zip(self.p_c, self._objects)):
                local_traj = self.local_traj_dict[obj.name]  # (J_i, T, sum J\i, N, dim)
                J_i, T, n_kpts, N, dim = local_traj.shape
                logging.info(f"the shape of the local traj on {obj.name} is {local_traj.shape}")
                if T > 1 and self.c.last_time_step_pce:
                    local_traj = local_traj[:, [-1]]
                    T = 1
                local_traj = local_traj.reshape((J_i * T * n_kpts, N, dim))

                pool = Pool(self.c.n_cpu)
                self.pca_res_dict[obj.name] = np.array(pool.map(pca, local_traj)).reshape((J_i, T, n_kpts, dim + 2, dim))

            np.savez(pca_res_file, **self.pca_res_dict)
            logging.info(f"pca results saved to {pca_res_file}")
        logging.info("done computing PCA")

        self.log(" principal manifold: linear ")
        for obj_idx, obj in enumerate(self._objects):
            logging.info(f"==== observing from object {obj.name} ====")

            # Note compute variability
            pca_res_var = self.pca_res_dict[obj.name][..., -1, :]
            if self.c.last_time_step_pce:
                pca_res_var = pca_res_var[:, [-1]]
                self.mask[obj_idx][:, :-1] = True
            pca_res_var = np.sqrt(pca_res_var) / obj.spatial_scale
            J_i, T, N_kpts, dim = pca_res_var.shape

            # Note: abstract p2p constraints based on variance norm
            pca_res_var_norm = np.linalg.norm(pca_res_var, axis=-1)  # (J_i, T, n_kpts)
            lv_idx = np.where(pca_res_var_norm < self.c.th_lv)
            if self.c.last_time_step_pce:
                self.mask[obj_idx][:, -1][(lv_idx[0], lv_idx[2])] = True
            else:
                self.mask[obj_idx][lv_idx] = True

            if len(lv_idx[0]) > 0:
                logging.info(f"[1] extracting p2p constraints from {len(lv_idx[0])} candidates")
                self.select_lv_candidate(lv_idx, pca_res_var_norm[lv_idx], obj_idx=obj_idx, constraint_type="p2p",
                                         assign_T=self.c.last_time_step_pce)
            else:
                logging.info("no viable p2p constraints found")

            # Note: abstract p2l, p2P constraints based on variance in certain direction
            pca_res_var_ma = np.ma.array(pca_res_var)
            pca_res_var_ma.mask = np.zeros_like(pca_res_var, dtype=bool)  # (J_i, T, n_kpts, dim)
            for d in range(1, dim):
                # allow p2P when N >=4, and allow p2l when N >= 3
                if (d == 2 and self.n_demo < 4) or (d == 1 and self.n_demo < 3):
                    break
                if self.c.last_time_step_pce:
                    for i_ in range(dim):
                        pca_res_var_ma.mask[..., i_] = self.mask[obj_idx][:, [-1]]
                else:
                    for i_ in range(dim):
                        pca_res_var_ma.mask[..., i_] = self.mask[obj_idx]

                if d == 1:
                    constraint_type = "p2l"
                    pm_linear_idx = np.ma.where(
                        (pca_res_var_ma[..., d] < self.c.th_linear_lv) &
                        (pca_res_var_ma[..., 0] > self.c.principal_var_ratio * pca_res_var_ma[..., d]) &
                        (pca_res_var_ma[..., 0] > self.c.th_linear_hv)
                    )
                    logging.info("looking for candidates of p2l constraints")
                    value_array = np.abs(pca_res_var[..., 0][pm_linear_idx])
                else:
                    constraint_type = "p2P"
                    pm_linear_idx = np.ma.where(
                        (pca_res_var_ma[..., 0] > self.c.principal_var_ratio * pca_res_var_ma[..., d]) &
                        (pca_res_var_ma[..., 0] > self.c.th_linear_hv) &
                        (pca_res_var_ma[..., 1] > self.c.principal_var_ratio * pca_res_var_ma[..., d]) &
                        (pca_res_var_ma[..., 1] > self.c.th_linear_hv) &
                        (pca_res_var_ma[..., 2] < self.c.th_linear_hv)
                    )
                    value_array = np.linalg.norm(pca_res_var[..., :2][pm_linear_idx], axis=-1)

                logging.info(
                    f"[{d + 1}] extracting {constraint_type} constraints from {len(pm_linear_idx[0])} candidates")
                if len(pm_linear_idx[0]) == 0:
                    logging.info(f"no viable {constraint_type} found")
                    continue

                self.select_lv_candidate(pm_linear_idx, value_array,
                                         obj_idx=obj_idx, constraint_type=constraint_type, assign_T=self.c.last_time_step_pce)

            idx_to_be_excluded = np.ma.where(np.abs(pca_res_var[..., 1]) < 4.2 * self.c.th_linear_lv)
            if self.c.last_time_step_pce:
                self.mask[obj_idx][:, -1][(idx_to_be_excluded[0], idx_to_be_excluded[2])] = True
            else:
                self.mask[obj_idx][idx_to_be_excluded] = True

    def nonlinear_constraint(self):
        """ extract nonlinear constraints, e.g. p2c, pcS using PME """
        logging.info("---" * 20 + " PME " + "---" * 20)
        pme_res_file = join(self.proc_path, "pme_result.npz")
        if os.path.isfile(pme_res_file) and not self.force_redo and False:
            logging.info(f"load PME results from {pme_res_file}")
            self.pme_res_dict = np.load(pme_res_file)
        else:
            for i, (idx_start_end, obj) in enumerate(zip(self.p_c, self._objects)):
                local_traj = self.local_traj_dict[obj.name][:, [-1]]  # (J_i, T, sum J\i, N, dim)
                idx_to_be_handled = np.where(np.invert(self.mask[i]))
                idx_to_be_handled = (idx_to_be_handled[0], np.zeros_like(idx_to_be_handled[0]), idx_to_be_handled[2])
                if len(idx_to_be_handled[0]) == 0:
                    logging.info("continue")
                    continue

                pca_res_var = self.pca_res_dict[obj.name][..., -1, :]
                pca_res_var = pca_res_var[:, [-1]]
                pca_res_var = np.sqrt(pca_res_var) / obj.spatial_scale
                # J_i, T, N_kpts, dim = pca_res_var.shape
                pca_res_var_norm = np.linalg.norm(pca_res_var, axis=-1)  # (J_i, T, n_kpts)
                value_to_compare = pca_res_var_norm[idx_to_be_handled]

                self.select_lv_candidate(idx_to_be_handled, value_to_compare, i, constraint_type='p2c', assign_T=True)

                for salient_idx in range(self._n_obj):
                    if salient_idx == i:
                        continue
                    search_id = i * self._n_obj + salient_idx
                    constraint_list_idx = self.constraints_index_lists[search_id]
                    for c_idx in constraint_list_idx:
                        if self.constraints[c_idx].type == "p2c":
                            c = self.constraints[c_idx]
                            frame_idx = c.local_frame_idx
                            kpts_idx = c.kpts_index
                            time_idx = 0
                            idx_tuple = (frame_idx, time_idx, kpts_idx)

                            data_points = local_traj[idx_tuple]
                            sol_opt_x, sol_opt_t, t_opt, std_stress, std_projection, embedding, scale, w, mu, sig, proj = \
                                pme_func(data_points, intrinsic_dim=1)

                            c.pm_sol_opt_x = sol_opt_x.tolist()
                            c.pm_sol_opt_t = sol_opt_t.tolist()
                            c.pm_embedding = embedding.tolist()
                            c.pm_t_opt = t_opt.tolist()
                            c.pm_std_stress = std_stress
                            c.pm_std_projection = std_projection
                            c.pm_intrinsic_dim = 1
                            c.pm_scaling = scale
                            c.density_mu = mu.tolist()
                            c.density_weights = w.tolist()
                            c.density_std_dev = sig
                            center_point = mapping(mu, 1, sol_opt_x, sol_opt_t, t_opt) * scale
                            c.pca_mean = center_point.tolist()

    def pce(self):
        self.initialize()
        self.frame_matching_and_projection()

        # Note construct masks for each object, initially not masked
        for idx, (idx_start_end, obj_name) in enumerate(zip(self.p_c, self.obj_list)):
            self.mask.append(np.zeros(self.local_traj_dict[obj_name].shape[:3], dtype=bool))
            if self.viz_debug:
                logging.info(f"visualizing the trajectories in local frames on {obj_name} before analyzing the data")
                idx_of_point_on_this_obj = np.arange(idx_start_end[0], idx_start_end[1])
                if self.c.dim == 2:
                    self.viz_local_traj_2d(idx, traj=self.local_traj_dict[obj_name], frame_mat=self.frame_mat_dict[obj_name],
                                           traj_i=self.t_c[idx_of_point_on_this_obj], pca_res=None)
                else:
                    self.viz_local_traj_3d(idx, traj=self.local_traj_dict[obj_name], frame_mat=self.frame_mat_dict[obj_name],
                                           traj_i=self.t_c[idx_of_point_on_this_obj], pca_res=None)

        IJ, T, N, dim = self.t_c.shape
        if N > 1:
            self.linear_constraints()
            if N > 10:
                self.nonlinear_constraint()

        # TODO check here
        all_obj_no_variation = np.all(np.invert(self.obj_variations))
        if all_obj_no_variation:
            self.obj_variations = np.ones_like(self.obj_variations, dtype=bool)
        if N == 1 or all_obj_no_variation:
            self.distance_criteria()

        self.abstracting()
        if self.viz:
            self.log(" visualizing the Geometric Constraints  ")
            for i, (idx_start_end, obj) in enumerate(zip(self.p_c, self._objects)):
                if i != self.master_idx:
                    continue
                idx = np.arange(idx_start_end[0], idx_start_end[1])
                traj_i = self.t_c[idx]  # (J_i, T, N, 2)
                pca_res = None if N == 1 else self.pca_res_dict[obj.name]
                if self.c.dim == 2:
                    self.viz_local_traj_2d(i, self.local_traj_dict[obj.name], self.frame_mat_dict[obj.name], traj_i, pca_res)
                else:
                    self.viz_local_traj_3d(i, self.local_traj_dict[obj.name], self.frame_mat_dict[obj.name], traj_i, pca_res)

        self.save()

    @staticmethod
    def log(log_str):
        logging.info(f"{log_str:-^150s}")

    def abstracting(self):
        self.log(" List of All Geometric Constraints ")
        for target_idx in range(self._n_obj):
            if target_idx == self.master_idx:
                self.log(f" On master object {self._objects[target_idx].name}")
            else:
                self.log(f" On slave object {self._objects[target_idx].name}, discarded ")
                continue
            for salient_idx in range(self._n_obj):
                if salient_idx == target_idx:
                    continue
                search_id = target_idx*self._n_obj+salient_idx
                constraint_list_idx = self.constraints_index_lists[search_id]

                if not len(constraint_list_idx) == self.constraints_count[target_idx, salient_idx]:
                    raise ValueError(f"{self.constraints_count[target_idx, salient_idx]} has different length to {constraint_list_idx}")
                for c_idx in constraint_list_idx:
                    logging.info(self.constraints[c_idx])
                self.log("")

    def save(self):
        c_path = create_path(join(self.path, "constraints"), remove_existing=True, force=True)
        self.log(f" Saving to {c_path}")
        for i, c in enumerate(self.constraints):
            if self.c.dim == 3:
                target_obj = self._objects[self.obj_list.index(c.target_obj)]
                salient_obj = self._objects[self.obj_list.index(c.salient_obj)]

                c.local_frame_config_array = self.theta_c[c.target_obj][c.local_frame_idx].tolist()
                c.local_frame_config_descriptors = target_obj.descriptors_all[target_obj.inlier_index[self.theta_d[c.target_obj][c.local_frame_idx]]].tolist()
                c.local_frame_config_coordinates = self.theta_p[c.target_obj][c.local_frame_idx].tolist()
                c.salient_obj_spatial_scale = salient_obj.spatial_scale
                c.kpts_descriptor = salient_obj.descriptors_all[[salient_obj.inlier_index[c.kpts_index]]].tolist()
                if c.type in ["p2p", "p2l", "p2P"]:
                    if self.n_demo == 1:
                        c.pca_mean = self.local_traj_dict[c.target_obj][c.local_frame_idx, c.time_step, c.kpts_index].flatten().tolist()  # (J_i-th, T, sum J\i, N, dim)
                    else:
                        pca_res = self.pca_res_dict[c.target_obj][c.local_frame_idx]  # (J_i-th, T, n_kpts, dim + 2, dim)
                        pca_time = -1 if self.c.last_time_step_pce else c.time_step
                        c.pca_components = pca_res[pca_time, c.kpts_index, :self.c.dim, :].tolist()
                        # TODO, need to project the mean onto the manifold, line, or plane, since the mean is not necessarily
                        #  on the manifold
                        c.pca_mean = pca_res[pca_time, c.kpts_index, self.c.dim, :].tolist()
                        c.pca_var = pca_res[pca_time, c.kpts_index, -1, :].tolist()

                # (J_i, T, sum J\i, N, dim) -> (N, T, dim)
                traj = self.local_traj_dict[c.target_obj][c.local_frame_idx, :, c.kpts_index].transpose((1, 0, 2))
                # Note: density
                data_points = traj[:, c.time_step]
                if c.type == "p2l":
                    # todo projection on to the line
                    line_vec = np.array(c.pca_components[0])
                    line_mean = np.array(c.pca_mean)
                    projection = (data_points - line_mean).dot(line_vec).reshape(-1, 1)
                    weights, mu, sig, label = hdmde(projection, n_cluster_min=1, alpha=0.1, max_comp=1)

                    c.density_weights = weights.tolist()
                    c.density_mu = mu.tolist()
                    c.density_std_dev = sig

                elif c.type == "p2P":
                    plane_xy = np.array(c.pca_components[:2])
                    plane_mean = np.array(c.pca_mean)
                    projection = (data_points - plane_mean).dot(plane_xy.T)

                    weights, mu, sig, label = hdmde(projection, n_cluster_min=1, alpha=0.9, max_comp=1)

                    c.density_weights = weights.flatten().tolist()
                    c.density_mu = mu.tolist()
                    c.density_std_dev = sig

                    lim_x = 84
                    lim_y = 40
                    n = 31
                    x, y = np.mgrid[-lim_x:lim_x:31j, -lim_y:lim_y:31j]
                    test_data = np.stack((x.ravel(), y.ravel())).T
                    prob = np.zeros(test_data.shape[0])
                    prob_derivative = np.zeros_like(test_data)
                    for j in range(mu.shape[0]):
                        prob_ = st.multivariate_normal.pdf(test_data, mu[j], sig ** 2) * weights[j]
                        prob += prob_
                        prob_derivative -= prob_.reshape(-1, 1) * (test_data - mu[j]) / (sig ** 2)

                # Note VMPs
                N, T, dim = traj.shape
                if c.type == "p2p":
                    vmp_dim = 3
                    c.vmp_goal = c.pca_mean
                elif c.type == "p2l":
                    vmp_dim = 1
                    traj = np.linalg.norm(np.cross(
                        (traj - np.array(c.pca_mean)), np.array(c.pca_components[0]), axis=-1),
                        axis=-1, keepdims=True
                    )
                    c.vmp_goal = [0]
                elif c.type == "p2P":
                    vmp_dim = 1
                    traj = (traj - np.array(c.pca_mean)).dot(np.array(c.pca_components[-1])).reshape(N, -1, 1)
                    c.vmp_goal = [0]
                elif c.type == "p2c":
                    sol_opt_x = np.array(c.pm_sol_opt_x)
                    sol_opt_t = np.array(c.pm_sol_opt_t)
                    embedding = np.array(c.pm_embedding)
                    t_opt = np.array(c.pm_t_opt)

                    traj = traj.reshape(-1, 3)

                    proj_intrinsic = projection_index(traj / c.pm_scaling,
                                                      embedding.mean(axis=0, keepdims=True),
                                                      sol_opt_x, sol_opt_t, t_opt, c.pm_intrinsic_dim)

                    proj_point = mapping(proj_intrinsic, d=c.pm_intrinsic_dim, sol_opt_x=sol_opt_x, sol_opt_t=sol_opt_t,
                                         t_opt=t_opt)
                    proj_point *= c.pm_scaling
                    stress = traj - proj_point
                    traj = np.linalg.norm(stress, axis=-1)
                    traj = traj.reshape((N, T, 1))
                    vmp_dim = 1
                    c.vmp_goal = [0]
                else:
                    raise NotImplementedError

                traj_path = create_path(join(c_path, f"traj_{i:>02d}"))
                time_stamps = np.tile(np.linspace(0, 1, T).reshape(-1, 1), (N, 1, 1))
                saving_traj = np.concatenate((time_stamps, traj), axis=-1)
                for n in range(N):
                    traj_filename = join(traj_path, f"demo_{n:>02d}.csv")
                    np.savetxt(traj_filename, saving_traj[n], delimiter=",")
                c.vmp_dim = vmp_dim
                c.vmp_kernel = 20
                vmp = VMP(c.vmp_dim, kernel_num=c.vmp_kernel)
                vmp.train(saving_traj)

                c.vmp_weights = vmp.get_weights().tolist()

                c_filename = join(c_path, f"constraint_{i:>02d}.yaml")
                dump_data_to_yaml(Constraints, c, c_filename)

    def load(self):
        preproc_file = join(self.proc_path, "preproc.npz")
        self.master_idx = load_dict_from_yaml(join(self.proc_path, "master_slave.yaml"))["master_idx"]
        if os.path.isfile(preproc_file) and not self.force_redo:
            logging.info(f"loading from {preproc_file}")
            data = np.load(preproc_file, allow_pickle=True)
            self.p_c, self.d_c, self.t_c = data["p_c"], data["d_c"], data['t_c']
            self.theta_c = load_npz_to_dict(join(self.proc_path, "theta_c.npz"))
            self.theta_d = load_npz_to_dict(join(self.proc_path, "theta_d.npz"))
            self.theta_p = load_npz_to_dict(join(self.proc_path, "theta_p.npz"))

        J, T, N, dim = self.t_c.shape
        local_traj_file = join(self.proc_path, "local_traj.npz")
        frame_mat_file = join(self.proc_path, "frame_mat.npz")
        if os.path.isfile(local_traj_file) and not self.force_redo:
            logging.info(f"loading from {local_traj_file}")
            traj_data = np.load(local_traj_file)
            frame_data = np.load(frame_mat_file)
            for obj_name in self.obj_list:
                self.frame_mat_dict[obj_name] = frame_data[obj_name]
                self.local_traj_dict[obj_name] = traj_data[obj_name]

        pca_res_file = join(self.proc_path, "pca_result.npz")
        if os.path.isfile(pca_res_file) and not self.force_redo:
            logging.info(f"load PCA results from {pca_res_file}")
            self.pca_res_dict = np.load(pca_res_file)

        c_path = join(self.path, "constraints")
        c_files = get_ordered_files(c_path, "constraint_", depth="/*")
        for f in c_files:
            c = load_dataclass(Constraints, f)
            if c.target_obj == self._objects[self.master_idx].name:
                logging.info(c)
                self.constraints.append(c)

        if self.viz:
            self.log(" visualizing the Geometric Constraints  ")
            for i, (idx_start_end, obj) in enumerate(zip(self.p_c, self._objects)):
                if i != self.master_idx:
                    continue
                idx = np.arange(idx_start_end[0], idx_start_end[1])
                traj_i = self.t_c[idx]  # (J_i, T, N, 2)
                pca_res = None if N == 1 else self.pca_res_dict[obj.name]
                if self.c.dim == 2:
                    self.viz_local_traj_2d(i, self.local_traj_dict[obj.name], self.frame_mat_dict[obj.name], traj_i, pca_res)
                else:
                    self.viz_local_traj_3d(i, self.local_traj_dict[obj.name], self.frame_mat_dict[obj.name], traj_i, pca_res)

    def select_lv_candidate(self, idx_tuple, value_array, obj_idx=None, constraint_type='p2p', assign_T=False):
        """
        Args:
            idx_tuple: 3 element tuple of indices, each dim means (J_i, T, n_kpts)
            value_array: for p2p: this is the variance norm, the var_norm of all points corresponds to idx_tuple
                for p2l/P/c/S this is the absolute variance in certain direction
            th_dist_kpt: the threshold of the distance between clustering of kpts on a reference tool (canonical shape)
            obj_idx: the index of the target object
            assign_T: if set to True, we assign T to the corresponding "time_step" entry in the constraints

        Returns:

        """
        if "hand" in self._objects[obj_idx].name:
            return

        # Note: find cluster in time scale
        logging.info("(1) time clustering")

        unique_t = np.unique(idx_tuple[1])
        split_idx = np.where((unique_t[1:] - unique_t[:-1]) >= self.c.th_time_cluster)[0] + 1
        split_idx = [0, ] + split_idx.tolist() + [unique_t.shape[0], ]
        time_clusters = [unique_t[np.arange(split_idx[j], split_idx[j + 1])] for j in range(len(split_idx) - 1)]

        for i_tc, tc in enumerate(time_clusters):
            # get the indices of the 3 lv_idx arrays that are clustered in the same time chunk
            logging.info(f"-- in {i_tc + 1}-th of {len(time_clusters)} time clustering")
            element_idx_in_this_t_cluster = np.concatenate([np.where(idx_tuple[1] == t)[0] for t in tc])
            # Note that element_idx is the index of the index of the key points, it is used to mask the idx_tuple
            #  we then get the actual kpt index and cluster them according to their coordinates on each primitive object
            unique_kpts_idx = np.unique(idx_tuple[2][element_idx_in_this_t_cluster])

            # handle the kpts on each object separately
            kpts_idx_from = 0
            for idx in range(self._n_obj):
                if idx == obj_idx or not self.obj_variations[idx]:
                    continue
                logging.info(f"-- (2) coordinate clustering for {self.obj_list[idx]}")
                obj = self._objects[idx]  # type: Tool
                sampled_kpts_coordinate = obj.get_sample_point_coordinates()

                # Note: find which points belong to this object
                kpts_idx_to = kpts_idx_from + sampled_kpts_coordinate.shape[0]
                unique_kpts_idx_on_this_obj = unique_kpts_idx[
                    np.where(
                        (kpts_idx_from <= unique_kpts_idx) & (unique_kpts_idx < kpts_idx_to)
                    )[0]
                ] - kpts_idx_from

                if len(unique_kpts_idx_on_this_obj) == 0:
                    kpts_idx_from = kpts_idx_to
                    continue
                ratio = len(unique_kpts_idx_on_this_obj) / self._objects[idx].n_sample_pts
                logging.info(f"-- -- the ratio of the number unique kpts on {self._objects[idx].name} is {ratio}")
                if constraint_type == "p2p" and ratio > 0.9:
                    self.obj_variations[idx] = False
                    logging.info(f"-- -- ratio = {ratio:>.2f}. object {self._objects[idx].name} doesn't have any variation, skip")
                    return

                # Note: fetch coordinates on the primitive object and fit the clustering model
                selected_sampled_kpts_coordinate = sampled_kpts_coordinate[unique_kpts_idx_on_this_obj]
                if unique_kpts_idx_on_this_obj.shape[0] == 1:
                    n_clusters = 1
                    kpts_clusters = np.array([0])
                else:
                    # Note: initialize the clustering algo based on the spatial scale of the object
                    model = AgglomerativeClustering(distance_threshold=obj.spatial_scale * self.c.th_dist_ratio,
                                                    linkage='average', n_clusters=None)
                    kpts_clusters = model.fit_predict(selected_sampled_kpts_coordinate)
                    n_clusters = model.n_clusters_

                for c in range(n_clusters):
                    logging.info(f"-- -- {c+1}-th of {n_clusters} coordinate clusters on {obj.name}")
                    unique_kpts_idx_in_this_cluster = unique_kpts_idx_on_this_obj[np.where(kpts_clusters == c)[0]]

                    # Note new
                    unique_kpts_min_dist_to_their_origin = np.zeros(len(unique_kpts_idx_in_this_cluster))
                    value_array_min = np.zeros_like(unique_kpts_min_dist_to_their_origin)
                    value_array_max = np.zeros_like(unique_kpts_min_dist_to_their_origin)
                    value_array_argmin = []
                    value_array_argmax = []
                    unique_kpts_all_dist_to_their_origin_list = []
                    list_of_element_idx_of_all_kpt_in_this_cluster = []

                    for i_, t_ in enumerate(unique_kpts_idx_in_this_cluster):
                        list_of_element_idx_of_one_kpt_in_this_cluster = np.where(idx_tuple[2] == (t_ + kpts_idx_from))[0]

                        if len(list_of_element_idx_of_one_kpt_in_this_cluster) == 0:
                            continue

                        list_of_element_idx_of_all_kpt_in_this_cluster.append(list_of_element_idx_of_one_kpt_in_this_cluster)
                        frame_idx_ = idx_tuple[0][list_of_element_idx_of_one_kpt_in_this_cluster]
                        time_idx_ = idx_tuple[1][list_of_element_idx_of_one_kpt_in_this_cluster] + (self.total_time_steps - 1 if assign_T else 0.0)
                        kpt_idx_ = idx_tuple[2][list_of_element_idx_of_one_kpt_in_this_cluster]
                        kpt_coord_in_corresponding_local_frames = self.local_traj_dict[self.obj_list[obj_idx]][
                            (frame_idx_, time_idx_, kpt_idx_)
                        ]  # (n_kpts, N, dim)
                        kpt_dist_to_their_origins = np.linalg.norm(kpt_coord_in_corresponding_local_frames, axis=-1).mean(axis=-1)
                        unique_kpts_all_dist_to_their_origin_list.append(kpt_dist_to_their_origins)
                        unique_kpts_min_dist_to_their_origin[i_] = kpt_dist_to_their_origins.min()

                        value_array_chunk = value_array[list_of_element_idx_of_one_kpt_in_this_cluster]
                        value_array_min[i_] = value_array_chunk.min()
                        value_array_max[i_] = value_array_chunk.max()
                        value_array_argmin.append(value_array_chunk.argmin())
                        value_array_argmax.append(value_array_chunk.argmax())

                    sort_dist = unique_kpts_min_dist_to_their_origin.argsort()
                    ranks_dist = np.empty_like(sort_dist)
                    ranks_dist[sort_dist] = np.arange(len(sort_dist))
                    temp_min, temp_max = unique_kpts_min_dist_to_their_origin.min(), unique_kpts_min_dist_to_their_origin.max()
                    score_dist = unique_kpts_min_dist_to_their_origin - temp_min if temp_max == temp_min else \
                        (unique_kpts_min_dist_to_their_origin - temp_min) / (temp_max - temp_min)

                    sort_value = value_array_min.argsort()
                    ranks_value = np.empty_like(sort_value)
                    ranks_value[sort_value] = np.arange(len(sort_value))
                    temp_min, temp_max = value_array_min.min(), value_array_min.max()
                    score_value = value_array_min - temp_min if temp_max == temp_min else \
                        (value_array_min - temp_min) / (temp_max - temp_min)

                    temp_min, temp_max = value_array_max.min(), value_array_max.max()
                    score_value_max = temp_max - value_array_max if temp_max == temp_min else \
                        (temp_max - value_array_max) / (temp_max - temp_min)

                    masked_time_step = tc[:, None] + (self.total_time_steps - 1 if assign_T else 0.0)

                    candidate_coords = sampled_kpts_coordinate[unique_kpts_idx_in_this_cluster]
                    dist = np.linalg.norm(
                        np.expand_dims(candidate_coords, 1) - np.expand_dims(sampled_kpts_coordinate, 0),
                        axis=-1)
                    excluded_idx = np.unique(
                        np.where(dist < 0.1 * obj.spatial_scale)[1]
                    )

                    self.mask[obj_idx][:, masked_time_step, excluded_idx + kpts_idx_from] = True

                    if constraint_type == "p2p":
                        selected_kpts_group = (score_value + score_dist).argmin()
                        selected_ele_idx = list_of_element_idx_of_all_kpt_in_this_cluster[selected_kpts_group][
                            unique_kpts_all_dist_to_their_origin_list[selected_kpts_group].argmin()
                        ]
                    elif constraint_type in ["p2l", "p2P"]:
                        selected_kpts_group = (score_value_max + score_dist).argmin()
                        ele_index_ = list_of_element_idx_of_all_kpt_in_this_cluster[selected_kpts_group]
                        available_frame_idx = idx_tuple[0][ele_index_]
                        for c_ in self.constraints_index_lists[obj_idx*self._n_obj+idx]:
                            if self.constraints[c_].type == "p2p":
                                index_ = np.where(available_frame_idx == self.constraints[c_].local_frame_idx)[0]
                                if len(index_) > 0:
                                    selected_ele_idx = ele_index_[index_][0]
                                    # ic(selected_ele_idx)
                                    break
                        else:
                            selected_ele_idx = ele_index_[
                                # value_array_argmax[score_selected_kpts_group]
                                unique_kpts_all_dist_to_their_origin_list[selected_kpts_group].argmin()
                            ]
                    elif constraint_type in ["p2c", "p2S"]:
                        # selected_kpts_group = (score_value_max + score_dist).argmin()
                        selected_kpts_group = score_value_max.argmin()
                        ele_index_ = list_of_element_idx_of_all_kpt_in_this_cluster[selected_kpts_group]
                        available_frame_idx = idx_tuple[0][ele_index_]
                        for c_ in self.constraints_index_lists[obj_idx * self._n_obj + idx]:
                            if self.constraints[c_].type == "p2p":
                                index_ = np.where(available_frame_idx == self.constraints[c_].local_frame_idx)[0]
                                # logging.info(
                                #     f"we have a p2p {self.constraints[c_].local_frame_idx}, {available_frame_idx}")
                                if len(index_) > 0:
                                    selected_ele_idx = ele_index_[index_][0]
                                    break
                        else:
                            selected_ele_idx = ele_index_[
                                unique_kpts_all_dist_to_their_origin_list[selected_kpts_group].argmin()
                            ]
                    else:
                        raise NotImplementedError
                    selected_frame_index = idx_tuple[0][selected_ele_idx]
                    selected_time_index = self.total_time_steps - 1 if assign_T else idx_tuple[1][selected_ele_idx]
                    selected_kpt_index = idx_tuple[2][selected_ele_idx]

                    # Note end new

                    # Note: construct the constraints
                    constraint = Constraints()
                    constraint.idx = len(self.constraints)
                    constraint.type = constraint_type
                    constraint.target_obj = self.obj_list[obj_idx]
                    constraint.salient_obj = self.obj_list[idx]
                    self.constraints_count[obj_idx, idx] += 1
                    self.constraints_index_lists[obj_idx*self._n_obj+idx].append(constraint.idx)

                    constraint.local_frame_idx = selected_frame_index
                    constraint.time_step = selected_time_index

                    selected_kpt_index_on_this_obj = selected_kpt_index - kpts_idx_from
                    constraint.kpts_index = selected_kpt_index_on_this_obj

                    self.constraints.append(constraint)
                    logging.info(f"-- -- -- {constraint}")

                    # Note: visualize
                    if self.viz_debug:
                        kpts_coordinates_in_this_cluster = sampled_kpts_coordinate[excluded_idx]
                        if self.c.dim == 2:
                            # ic(excluded_idx)
                            ax = plt.figure().add_subplot(1, 1, 1)
                            ax.scatter(sampled_kpts_coordinate[:, 0], sampled_kpts_coordinate[:, 1],
                                       alpha=0.5, c='k', label="sampled points")
                            ax.scatter(selected_sampled_kpts_coordinate[:, 0], selected_sampled_kpts_coordinate[:, 1],
                                       marker='x', s=100, c='b', label=f"points on {obj.name}")
                            ax.scatter(kpts_coordinates_in_this_cluster[:, 0], kpts_coordinates_in_this_cluster[:, 1],
                                       s=80, color="yellow", alpha=0.8, label="points in this cluster")
                            ax.set_title(f"{c + 1}-th result of {n_clusters} clusters for {constraint_type}")
                            set_2d_equal_auto(ax)
                            ax.set_axis_off()
                            plt.legend()
                            plt.show()
                        else:
                            ax = plt.figure().add_subplot(1, 1, 1, projection='3d')
                            ax.scatter3D(sampled_kpts_coordinate[:, 0], sampled_kpts_coordinate[:, 1], sampled_kpts_coordinate[:, 2],
                                         alpha=1, c='k', label="sampled points")
                            ax.scatter3D(selected_sampled_kpts_coordinate[:, 0], selected_sampled_kpts_coordinate[:, 1], selected_sampled_kpts_coordinate[:, 2],
                                         marker='x', s=100, c='b', label=f"points on {obj.name}")
                            ax.scatter3D(kpts_coordinates_in_this_cluster[:, 0], kpts_coordinates_in_this_cluster[:, 1], kpts_coordinates_in_this_cluster[:, 2],
                                         s=80, color="yellow", alpha=0.8, label="points in this cluster")
                            ax.set_title(f"{c+1}-th result of {n_clusters} clusters for {constraint_type}")
                            set_3d_equal_auto(ax)
                            ax.set_axis_off()
                            plt.legend()
                            plt.show()

                kpts_idx_from = kpts_idx_to
                logging.info(f"-- -- done analyzing {self.obj_list[idx]} in local frames of {self.obj_list[obj_idx]} for {constraint_type}")

    def viz_local_traj_2d(self, idx, traj, frame_mat, traj_i, pca_res=None):
        """
        visualize the N demonstrations in local frame
        Args:
            idx: the index of obj
            traj: (J_i, T, sum J\i, N, dim)
            frame_mat: (J_i, T, N, 2, 3)
            traj_i: the traj of target object (J_i, T, N, 2)
            pca_res: (J_i, T, n_kpts, dim+2, dim)
        """
        obj = self._objects[idx]
        J_i, T, n_kpts, N, dim = traj.shape

        idx_const_defined_on_object_i = [j for j in range(len(self.constraints)) if self.constraints[j].target_obj == obj.name]
        C = len(idx_const_defined_on_object_i)
        arrow_scaling = 1
        var_scaling = 1

        # figure
        fig = plt.figure(figsize=(16, 9))
        spec = fig.add_gridspec(ncols=4, nrows=5)
        ax0 = fig.add_subplot(spec[:4, :])
        plt.subplots_adjust(left=0.05, top=0.95, right=0.95, bottom=0.05)
        cmap = cm.get_cmap("rainbow")

        if C > 0:
            c = self.constraints[idx_const_defined_on_object_i[0]]
            lv_j_i, lv_t, lv_kpt = c.local_frame_idx, c.time_step, c.kpts_index
            ax0.set_title(f"Constraint Visualizer | on obj {self.obj_list[idx]} | type: {c.type}")
        else:
            lv_j_i, lv_t, lv_kpt = 0, 0, 0
            ax0.set_title(f"Trajectory Visualizer | on obj {self.obj_list[idx]}")

        patch_list_traj = []
        patch_list_salient_shape = []
        patch_list_target_shape = []
        patch_list_salient_polygon = []

        alpha_target_ = 0.2
        alpha_salient_ = 0.3
        alpha_frame_ = 1.0
        alpha_traj = 0.5
        alpha_pca_ = 1.0
        alpha_curve = 0.5
        self.alpha_target = alpha_target_
        self.alpha_salient = alpha_salient_
        self.trail_alphas = np.ones(N, dtype=float)

        # Note: the selected keypoints, the datapoints used to show PCA results
        data_point_line = ax0.scatter(
            traj[lv_j_i, lv_t, lv_kpt, :, 0], traj[lv_j_i, lv_t, lv_kpt, :, 1],
            marker="o", s=150, facecolor='yellow', edgecolor="k", alpha=0.6, zorder=5, label="keypoints"
        )

        for n in range(N):
            color_ratio = 0 if N == 1 else n/(N-1)

            # Note: transform sampled points on target to local frames
            sampled_pts = traj_i[:, lv_t, n, :]  # (J_i, dim)
            transformed_pts = np.einsum("ij,kj->ki", frame_mat[lv_j_i, lv_t, n, :, :self.c.dim], sampled_pts) - frame_mat[lv_j_i, lv_t, n, :, -1]  # (J_i, dim)
            patch_list_target_shape.append(
                obj.plot_patch(
                    ax0, transformed_pts[:obj.n_polygon_vertices], alpha=alpha_target_, linewidth=2,
                    face_color=cmap(color_ratio), edge_color=cmap(color_ratio))
            )

            # Note: plot trajectories
            line,  = ax0.plot(
                traj[lv_j_i, :, lv_kpt, n, 0], traj[lv_j_i, :, lv_kpt, n, 1],
                label=f"{n}", alpha=alpha_traj, linewidth=3, color=cmap(color_ratio), linestyle="--"
            )
            patch_list_traj.append(line)

            # Note: plot sampled points on other salient objects
            kpts_start_idx = 0
            for i, other_obj in enumerate(self._objects):
                if i == idx:
                    continue
                else:
                    kpts_end_idx = kpts_start_idx + other_obj.n_sample_pts
                    slice = np.arange(kpts_start_idx, kpts_end_idx)
                    kpts_coords = traj[lv_j_i, lv_t, slice, n, :]
                    # sampled points scatter plot
                    patch_list_salient_shape.append(
                        ax0.scatter(kpts_coords[..., 0], kpts_coords[..., 1], alpha=alpha_salient_, s=10, color=cmap(color_ratio))
                    )
                    # ploygon patch
                    patch_list_salient_polygon.append(
                        other_obj.plot_patch(ax0, kpts_coords[:other_obj.n_polygon_vertices], alpha=alpha_salient_, linewidth=1,
                                             face_color=cmap(color_ratio), edge_color=cmap(color_ratio), linestyle="--")
                    )

        # Note: lines to lot principal axis, which is scaled by the explained variance
        pca_colors_ = ['r', 'limegreen', 'b']
        if pca_res is not None:
            pca_time = -1 if self.c.last_time_step_pce and lv_t == T-1 else lv_t
            pca_components, pca_mean, pca_exp_var = pca_res[lv_j_i, pca_time, lv_kpt, :dim, :], pca_res[lv_j_i, pca_time, lv_kpt, dim, :], pca_res[lv_j_i, pca_time, lv_kpt, -1, :]
            pca_components_line = []
            for i, (comp, var) in enumerate(zip(pca_components, pca_exp_var)):
                # scale component by its explained variance
                comp = comp * var * var_scaling + pca_mean
                line, = ax0.plot([pca_mean[0], comp[0]], [pca_mean[1], comp[1]],
                                 linewidth=5, color=pca_colors_[i], alpha=alpha_pca_, label="PCA")
                pca_components_line.append(line)

        draw_frame_2d(ax0, np.array([0, 0, 0]), scale=50.0, alpha=1.0)

        ax0.legend()
        set_2d_equal_auto(ax0)
        ax0.set_xticks([])
        ax0.set_yticks([])
        ax0.set_aspect('equal', adjustable='box')

        # Note: sliders (frame index, time, kpts_index, demonstration, constraint index)
        # (J_i, T, sum J\i, N, dim)
        J_i_slider = Slider(ax=plt.axes([0.35, 0.19, 0.5, 0.02]), label='J_i', valmin=1, valmax=J_i, valinit=lv_j_i+1, valstep=1)
        if T > 1:
            T_slider = Slider(ax=plt.axes([0.35, 0.15, 0.5, 0.02]), label='T', valmin=1, valmax=T, valinit=lv_t+1, valstep=1)
        n_kpts_slider = Slider(ax=plt.axes([0.35, 0.11, 0.5, 0.02]), label='n_kpts', valmin=1, valmax=n_kpts, valinit=lv_kpt+1, valstep=1)
        N_slider = Slider(ax=plt.axes([0.35, 0.07, 0.5, 0.02]), label='N', valmin=0, valmax=N, valinit=0, valstep=1)

        if C > 0:
            C_slider = Slider(ax=plt.axes([0.35, 0.03, 0.5, 0.02]), label='C', valmin=1, valmax=C, valinit=1, valstep=1)

        # Note: buttons
        ax_alpha_target = plt.axes([0.1, 0.03, 0.1, 0.075])
        ax_alpha_salient = plt.axes([0.1, 0.1, 0.1, 0.075])

        self.prev_c_idx = 0
        self.prev_demo_idx = 0
        def update(val):
            _j_i = int(J_i_slider.val) - 1
            _t = 0 if T == 1 else int(T_slider.val) - 1
            _n_kpts = int(n_kpts_slider.val) - 1
            if C > 0 and (int(C_slider.val) - 1 != self.prev_c_idx):
                _c = int(C_slider.val) - 1
                self.prev_c_idx = _c
                c = self.constraints[idx_const_defined_on_object_i[_c]]
                ax0.set_title(f"c: {c.type}")
                _j_i, _t, _n_kpts = c.local_frame_idx, c.time_step, c.kpts_index
                J_i_slider.set_val(_j_i+1)
                if T > 1:
                    T_slider.set_val(_t+1)
                n_kpts_slider.set_val(_n_kpts+1)

            data_point_line.set_offsets(traj[_j_i, _t, _n_kpts])
            for n in range(N):  # (J_i, T, sum J\i, N, dim)
                sampled_pts = traj_i[:, _t, n, :]  # (J_i, dim)
                # frame_mat: (J_i, T, N, 2, 3)
                transformed_pts = np.einsum("ij,kj->ki", frame_mat[_j_i, _t, n, :, :self.c.dim], sampled_pts) - \
                                  frame_mat[_j_i, _t, n, :, -1]  # (J_i, 2)
                patch_list_target_shape[n].set_xy(transformed_pts[:obj.n_polygon_vertices])

                patch_list_traj[n].set_data(traj[_j_i, :, _n_kpts, n, 0], traj[_j_i, :, _n_kpts, n, 1])
                kpts_start_idx = 0
                for i, other_obj in enumerate(self._objects):
                    if i == idx:
                        continue
                    else:
                        kpts_end_idx = kpts_start_idx + other_obj.n_sample_pts
                        slice = np.arange(kpts_start_idx, kpts_end_idx)
                        kpts_coords = traj[_j_i, _t, slice, n, :]
                        patch_list_salient_shape[n].set_offsets(kpts_coords)
                        patch_list_salient_polygon[n].set_xy(kpts_coords[:other_obj.n_polygon_vertices])

            _n = int(N_slider.val)
            if _n != self.prev_demo_idx:
                self.prev_demo_idx = _n
                if _n == 0:
                    for n in range(N):
                        patch_list_salient_shape[n].set_alpha(alpha_salient_)
                        patch_list_target_shape[n].set_alpha(alpha_target_)
                        patch_list_salient_polygon[n].set_alpha(alpha_target_)
                else:
                    for n in range(N):
                        if n == _n - 1:
                            patch_list_salient_shape[n].set_alpha(alpha_salient_)
                            patch_list_target_shape[n].set_alpha(alpha_target_)
                            patch_list_salient_polygon[n].set_alpha(alpha_target_)
                        else:
                            patch_list_salient_shape[n].set_alpha(0.01)
                            patch_list_target_shape[n].set_alpha(0.01)
                            patch_list_salient_polygon[n].set_alpha(0.01)

            # pca_res: (J_i, T, n_kpts, dim+2, dim)
            if pca_res is not None:
                pca_time = -1 if self.c.last_time_step_pce else _t
                pca_components = pca_res[_j_i, pca_time, _n_kpts, :dim, :]
                pca_mean = pca_res[_j_i, pca_time, _n_kpts, dim, :]
                pca_exp_var = pca_res[_j_i, pca_time, _n_kpts, -1, :]
                for i, (comp, var) in enumerate(zip(pca_components, pca_exp_var)):
                    comp = comp * var * var_scaling + pca_mean
                    pca_components_line[i].set_data([pca_mean[0], comp[0]], [pca_mean[1], comp[1]])

            fig.canvas.draw_idle()

        def update_target_alpha(event):
            if self.alpha_target == alpha_target_:
                self.alpha_target = 0
            else:
                self.alpha_target = alpha_target_
            for n in range(N):
                patch_list_target_shape[n].set_alpha(self.alpha_target)
            if event.inaxes is not None:
                event.inaxes.figure.canvas.draw_idle()

        def update_salient_alpha(event):
            if self.alpha_salient == alpha_salient_:
                self.alpha_salient = 0
            else:
                self.alpha_salient = alpha_salient_
            for n in range(N):
                patch_list_salient_shape[n].set_alpha(self.alpha_salient)
            if event.inaxes is not None:
                event.inaxes.figure.canvas.draw_idle()

        target_alpha_button = Button(ax_alpha_target,"Target Alpha")
        salient_alpha_button = Button(ax_alpha_salient, "Salient alpha")
        target_alpha_button.on_clicked(update_target_alpha)
        salient_alpha_button.on_clicked(update_salient_alpha)

        J_i_slider.on_changed(update)
        if T > 1:
            T_slider.on_changed(update)
        if C > 0:
            C_slider.on_changed(update)
        n_kpts_slider.on_changed(update)
        N_slider.on_changed(update)

        ax0.set_axis_off()
        plt.show()

    def viz_local_traj_3d(self, idx, traj, frame_mat, traj_i, pca_res=None):
        """
        visualize the N demonstrations in local frame
        Args:
            idx: the index of obj
            traj: (J_i, T, sum J\i, N, dim)
            frame_mat: (J_i, T, N, 3, 4)
            traj_i: the traj of target object (J_i, T, N, 2)
            pca_res: (J_i, T, n_kpts, dim+2, dim)
        """
        obj = self._objects[idx]
        J_i, T, n_kpts, N, dim = traj.shape

        idx_const_defined_on_object_i = [j for j in range(len(self.constraints)) if self.constraints[j].target_obj == obj.name]
        C = len(idx_const_defined_on_object_i)
        arrow_scaling = 100
        var_scaling = 0.1

        # figure
        fig = plt.figure(figsize=(16, 9))
        spec = fig.add_gridspec(ncols=4, nrows=5)
        ax0 = fig.add_subplot(spec[:4, :], projection="3d")
        plt.subplots_adjust(left=0.05, top=0.95, right=0.95, bottom=0.05)
        cmap = cm.get_cmap("brg")

        if C > 0:
            c = self.constraints[idx_const_defined_on_object_i[0]]
            lv_j_i, lv_t, lv_kpt = c.local_frame_idx, c.time_step, c.kpts_index
            ax0.set_title(f"Constraint Visualizer | on obj {self.obj_list[idx]} | type: {c.type}")
        else:
            lv_j_i, lv_t, lv_kpt = 0, 0, 0
            ax0.set_title(f"Trajectory Visualizer | on obj {self.obj_list[idx]}")

        patch_list_traj = []
        patch_list_salient_shape = []
        patch_list_target_shape = []
        patch_list_hand_master = []
        patch_list_hand_slave = []
        patch_list_curve = []

        alpha_target_ = 0.2
        alpha_salient_ = 0.3
        alpha_frame_ = 1.0
        alpha_traj = 0.5
        alpha_pca_ = 1.0
        alpha_curve = 0.5
        self.alpha_target = alpha_target_
        self.alpha_salient = alpha_salient_
        self.trail_alphas = np.ones(N, dtype=float)

        total_num_of_slave_hands = 0
        for i, name in enumerate(self.obj_list):
            if i == idx:
                continue
            else:
                if "hand" in name:
                    total_num_of_slave_hands += 1

        for n in range(N):
            color_ratio = 0 if N == 1 else n/(N-1)

            # Note: transform sampled points on target to local frames
            sampled_pts = traj_i[:, lv_t, n, :]  # (J_i, 3)
            transformed_pts = np.einsum("ij,kj->ki", frame_mat[lv_j_i, lv_t, n, :, :self.c.dim], sampled_pts) - frame_mat[lv_j_i, lv_t, n, :, -1]  # (J_i, 3)
            # if obj.name == "hand":
            if "hand" in obj.name:
                patch_list_hand_master.append(obj.draw(transformed_pts, ax0, collections=None, color=cmap(color_ratio)))
            patch_list_target_shape.append(
                ax0.scatter3D(
                    transformed_pts[:, 0], transformed_pts[:, 1], transformed_pts[:, 2],
                    s=10, alpha=alpha_target_, color=cmap(color_ratio)
                )
            )  # edgecolors=target_shape_edge_color,

            # Note: plot trajectories
            line,  = ax0.plot(
                traj[lv_j_i, :, lv_kpt, n, 0], traj[lv_j_i, :, lv_kpt, n, 1], traj[lv_j_i, :, lv_kpt, n, 2],
                label=f"{n}", alpha=alpha_traj, linewidth=3, color=cmap(color_ratio), linestyle="--"
            )
            patch_list_traj.append(line)

            # Note: plot sampled points on other salient objects
            kpts_start_idx = 0
            for i, other_obj in enumerate(self._objects):
                if i == idx:
                    continue
                else:
                    kpts_end_idx = kpts_start_idx + other_obj.n_sample_pts
                    slice = np.arange(kpts_start_idx, kpts_end_idx)
                    kpts_coords = traj[lv_j_i, lv_t, slice, n, :]
                    if "hand" in other_obj.name:
                        patch_list_hand_slave.append(other_obj.draw(kpts_coords, ax0, collections=None, color=cmap(color_ratio)))

                    # sampled points scatter plot
                    patch_list_salient_shape.append(ax0.scatter3D(kpts_coords[..., 0], kpts_coords[..., 1], kpts_coords[..., 2],
                                                     alpha=alpha_salient_, s=5, color=cmap(color_ratio), zorder=-1))
                    kpts_start_idx = kpts_end_idx

        set_3d_equal_auto(ax0)

        # Note: lines to plot principal axis, which is scaled by the explained variance
        pca_colors_ = ['r', 'limegreen', 'b']
        if pca_res is not None:
            pca_time = -1 if self.c.last_time_step_pce and lv_t == T-1 else lv_t
            pca_components, pca_mean, pca_exp_var = pca_res[lv_j_i, pca_time, lv_kpt, :dim, :], pca_res[lv_j_i, pca_time, lv_kpt, dim, :], pca_res[lv_j_i, pca_time, lv_kpt, -1, :]
            patch_list_pca_components = []
            for i, (comp, var) in enumerate(zip(pca_components, pca_exp_var)):
                # scale component by its explained variance
                comp = comp * var * var_scaling + pca_mean
                line, = ax0.plot([pca_mean[0], comp[0]], [pca_mean[1], comp[1]], [pca_mean[2], comp[2]],
                                 linewidth=5, color=pca_colors_[i], alpha=alpha_pca_, label="PCA")
                patch_list_pca_components.append(line)

        draw_frame_3d(ax0, np.array([0, 0, 0, 0, 0, 0.]), scale=arrow_scaling, alpha=alpha_frame_)

        # Note: the datapoints used to show PCA results
        patch_selected_point = ax0.scatter3D(
            traj[lv_j_i, lv_t, lv_kpt, :, 0],
            traj[lv_j_i, lv_t, lv_kpt, :, 1],
            traj[lv_j_i, lv_t, lv_kpt, :, 2],
            s=350, color='yellow', edgecolor='k', alpha=1, zorder=10, label="keypoints")

        # Note: plot fake data as line holder for pme results
        line_curve, = ax0.plot(
            traj[lv_j_i, lv_t, lv_kpt, :, 0],
            traj[lv_j_i, lv_t, lv_kpt, :, 1],
            traj[lv_j_i, lv_t, lv_kpt, :, 2],
            label=f"curve", alpha=alpha_curve, linewidth=5, color='yellow', linestyle="-", zorder=10
        )
        patch_list_curve.append(line_curve)
        ax0.legend()

        # Note: sliders (frame index, time, kpts_index, constraint index)
        # (J_i, T, sum J\i, N, dim)
        J_i_slider = Slider(ax=plt.axes([0.35, 0.19, 0.5, 0.02]), label='J_i', valmin=1, valmax=J_i, valinit=lv_j_i+1, valstep=1)
        if T > 1:
            T_slider = Slider(ax=plt.axes([0.35, 0.15, 0.5, 0.02]), label='T', valmin=1, valmax=T, valinit=lv_t+1, valstep=1)
        n_kpts_slider = Slider(ax=plt.axes([0.35, 0.11, 0.5, 0.02]), label='n_kpts', valmin=1, valmax=n_kpts, valinit=lv_kpt+1, valstep=1)
        N_slider = Slider(ax=plt.axes([0.35, 0.07, 0.5, 0.02]), label='N', valmin=0, valmax=N, valinit=0, valstep=1)

        if C > 0:
            C_slider = Slider(ax=plt.axes([0.35, 0.03, 0.5, 0.02]), label='C', valmin=1, valmax=C, valinit=1, valstep=1)
            # ax_viz_all_constraint = plt.axes([0.1, 0.2, 0.1, 0.075])

        # Note: buttons
        ax_alpha_target = plt.axes([0.1, 0.03, 0.1, 0.075])
        ax_alpha_salient = plt.axes([0.1, 0.1, 0.1, 0.075])

        self.prev_c_idx = 0
        self.prev_demo_idx = 0
        def update(val):
            _j_i = int(J_i_slider.val) - 1
            _t = 0 if T == 1 else int(T_slider.val) - 1
            _n_kpts = int(n_kpts_slider.val) - 1
            draw_pme = False
            c = None  # type: Union[Constraints, None]
            alpha_curve_ = 0.0
            if C > 0 and (int(C_slider.val) - 1 != self.prev_c_idx):
                _c = int(C_slider.val) - 1
                self.prev_c_idx = _c
                c = self.constraints[idx_const_defined_on_object_i[_c]]
                ax0.set_title(f"c: {c.type}")
                if c.type in ["p2c", "p2S"]:
                    alpha_curve_ = 1.0
                    draw_pme = True

                _j_i, _t, _n_kpts = c.local_frame_idx, c.time_step, c.kpts_index
                J_i_slider.set_val(_j_i+1)
                if T > 1:
                    T_slider.set_val(_t+1)
                n_kpts_slider.set_val(_n_kpts+1)

            # ic(traj.shape, _j_i, J_i, obj.name, _n_kpts)
            patch_selected_point._offsets3d = (traj[_j_i, _t, _n_kpts, :, 0], traj[_j_i, _t, _n_kpts, :, 1], traj[_j_i, _t, _n_kpts, :, 2])
            for n in range(N):  # (J_i, T, sum J\i, N, dim)
                sampled_pts = traj_i[:, _t, n, :]  # (J_i, 3)
                # frame_mat: (J_i, T, N, 3, 4)
                transformed_pts = np.einsum("ij,kj->ki", frame_mat[_j_i, _t, n, :, :self.c.dim], sampled_pts) - \
                                  frame_mat[_j_i, _t, n, :, -1]  # (J_i, 2)
                patch_list_target_shape[n]._offsets3d = (transformed_pts[:, 0], transformed_pts[:, 1], transformed_pts[:, 2])

                if "hand" in obj.name:
                    obj.draw(transformed_pts, ax0, patch_list_hand_master[n])

                patch_list_traj[n].set_data(traj[_j_i, :, _n_kpts, n, 0], traj[_j_i, :, _n_kpts, n, 1])
                patch_list_traj[n].set_3d_properties(traj[_j_i, :, _n_kpts, n, 2])
                kpts_start_idx = 0

                salient_obj_id_in_patch_list = 0
                hand_id_in_path_list = 0
                for i, other_obj in enumerate(self._objects):
                    if i == idx:
                        continue
                    else:
                        kpts_end_idx = kpts_start_idx + other_obj.n_sample_pts
                        slice = np.arange(kpts_start_idx, kpts_end_idx)
                        kpts_coords = traj[_j_i, _t, slice, n, :]
                        salient_index_ = salient_obj_id_in_patch_list + n * (len(self.obj_list) - 1)
                        patch_list_salient_shape[salient_index_]._offsets3d = (kpts_coords[:, 0], kpts_coords[:, 1], kpts_coords[:, 2])
                        salient_obj_id_in_patch_list += 1

                        if "hand" in other_obj.name:
                            hand_index_ = hand_id_in_path_list * total_num_of_slave_hands + n
                            other_obj.draw(kpts_coords, ax0, patch_list_hand_slave[hand_index_])
                            hand_id_in_path_list += 1

                        kpts_start_idx += other_obj.n_sample_pts

            _n = int(N_slider.val)
            if _n != self.prev_demo_idx:
                self.prev_demo_idx = _n
                if _n == 0:
                    for n in range(N):
                        patch_list_salient_shape[n].set_alpha(alpha_salient_)
                        patch_list_target_shape[n].set_alpha(alpha_target_)
                        patch_list_hand_slave[n].set_alpha(alpha_target_)
                else:
                    for n in range(N):
                        if n == _n - 1:
                            patch_list_salient_shape[n].set_alpha(alpha_salient_)
                            patch_list_target_shape[n].set_alpha(alpha_target_)
                            patch_list_hand_slave[n].set_alpha(alpha_target_)
                        else:
                            patch_list_salient_shape[n].set_alpha(0.01)
                            patch_list_target_shape[n].set_alpha(0.01)
                            patch_list_hand_slave[n].set_alpha(0.01)

            set_3d_equal_auto(ax0)

            # pca_res: (J_i, T, n_kpts, dim+2, dim)
            if pca_res is not None:
                pca_time = -1 if self.c.last_time_step_pce else _t
                pca_components = pca_res[_j_i, pca_time, _n_kpts, :dim, :]
                pca_mean = pca_res[_j_i, pca_time, _n_kpts, dim, :]
                pca_exp_var = pca_res[_j_i, pca_time, _n_kpts, -1, :]
                for i, (comp, var) in enumerate(zip(pca_components, pca_exp_var)):
                    comp = comp * var * var_scaling + pca_mean
                    patch_list_pca_components[i].set_data(np.array([pca_mean[0], comp[0]]), np.array([pca_mean[1], comp[1]]))
                    patch_list_pca_components[i].set_3d_properties(np.array([pca_mean[2], comp[2]]))

            # Note: to plot the curve or surface manifold
            if draw_pme:
                sol_opt_x = np.array(c.pm_sol_opt_x)
                sol_opt_t = np.array(c.pm_sol_opt_t)
                embedding = np.array(c.pm_embedding)
                t_opt = np.array(c.pm_t_opt)
                t_test = np.arange(-3, 3, 0.05)
                x_test = mapping(t_test, d=c.pm_intrinsic_dim, sol_opt_x=sol_opt_x, sol_opt_t=sol_opt_t, t_opt=t_opt)
                x_test *= c.pm_scaling
                patch_list_curve[0].set_data(x_test[:, 0], x_test[:, 1])
                patch_list_curve[0].set_3d_properties(x_test[:, 2])
            patch_list_curve[0].set_alpha(alpha_curve_)

            fig.canvas.draw_idle()

        def update_target_alpha(event):
            if self.alpha_target == alpha_target_:
                self.alpha_target = 0
            else:
                self.alpha_target = alpha_target_
            for n in range(N):
                patch_list_target_shape[n].set_alpha(self.alpha_target)
            if event.inaxes is not None:
                event.inaxes.figure.canvas.draw_idle()

        def update_salient_alpha(event):
            if self.alpha_salient == alpha_salient_:
                self.alpha_salient = 0
            else:
                self.alpha_salient = alpha_salient_
            for n in range(N):
                patch_list_salient_shape[n].set_alpha(self.alpha_salient)
            if event.inaxes is not None:
                event.inaxes.figure.canvas.draw_idle()

        target_alpha_button = Button(ax_alpha_target,"Target Alpha")
        salient_alpha_button = Button(ax_alpha_salient, "Salient alpha")
        target_alpha_button.on_clicked(update_target_alpha)
        salient_alpha_button.on_clicked(update_salient_alpha)

        J_i_slider.on_changed(update)
        if T > 1:
            T_slider.on_changed(update)
        if C > 0:
            C_slider.on_changed(update)
        n_kpts_slider.on_changed(update)
        N_slider.on_changed(update)

        ax0.set_axis_off()
        plt.show()

    def viz_frame_matching_2d(self, tool_name: str, theta_c: np.ndarray, theta_d: np.ndarray, theta_p: np.ndarray):
        tool = Tool(name=tool_name, role="Tool")

        fig = plt.figure(figsize=(16, 9))
        spec = fig.add_gridspec(ncols=1, nrows=5)
        cmap = cm.get_cmap("bwr")
        tool.scale(np.array([1., 1.]))
        scaled_point_coordinates = tool.get_scaled_sample_point_coordinates(self.c.n_sample_pts_max)
        n_sample_pts = scaled_point_coordinates.shape[0]

        ax0 = fig.add_subplot(spec[:4, 0], aspect="equal")
        tool.plot_patch(ax0)
        patch = tool.plot_patch(ax0, face_color='palegreen', edge_color='limegreen')
        tool_line_original = ax0.scatter(tool.sample_pts[:, 0], tool.sample_pts[:, 1],
                                         c=cmap(np.linspace(0, 1, n_sample_pts)), cmap=cmap, s=50, alpha=0.1, zorder=3)
        neighbor_pts = ax0.scatter(tool.sample_pts[:20, 0], tool.sample_pts[:20, 1], c='green', s=50, marker="x", alpha=0.6, zorder=3.5)

        xlim_min, xlim_max = min(tool.sample_pts[:, 0]), max(tool.sample_pts[:, 0])
        ylim_min, ylim_max = min(tool.sample_pts[:, 1]), max(tool.sample_pts[:, 1])
        ax0.set_xlim(xlim_min-10, xlim_max+10)
        ax0.set_ylim(ylim_min-10, ylim_max+10)

        tool_line_scaled = ax0.scatter(scaled_point_coordinates[:, 0], scaled_point_coordinates[:, 1],
                                       c=cmap(np.linspace(0, 1, n_sample_pts)), cmap=cmap, s=50, alpha=0.5, zorder=5)

        draw_frame_2d(ax0, np.array([0, 0, 0]), scale=10.0, alpha=1.0)
        init_frame_arrow_list = draw_frame_2d(ax0, np.array([0, 0, 0]), scale=20.0, alpha=0.3)
        new_frame_arrow_list = draw_frame_2d(ax0, np.array([0, 0, 0]), scale=20.0, alpha=0.3)

        scale_x_slider = Slider(ax=plt.axes([0.35, 0.19, 0.5, 0.02]), label='scale_x', valmin=0.1, valmax=10, valinit=1, valstep=0.1)
        scale_y_slider = Slider(ax=plt.axes([0.35, 0.15, 0.5, 0.02]), label='scale_y', valmin=0.1, valmax=10, valinit=1, valstep=0.1)
        point_idx_slider = Slider(ax=plt.axes([0.35, 0.11, 0.5, 0.02]), label='idx', valmin=1, valmax=n_sample_pts, valinit=1, valstep=1)

        def update(val):
            scale = np.array([scale_x_slider.val, scale_y_slider.val])
            point_idx = int(point_idx_slider.val) - 1

            tool.scale(scale)
            patch.set_xy(tool.pts)
            new_coordinates = tool.get_scaled_sample_point_coordinates(self.c.n_sample_pts_max)
            # current_point_init_config = init_frame_config_list[point_idx]
            neighbor_idx = theta_d[point_idx].flatten()

            new_config = find_local_frame(dim=2, new_point_coordinates=new_coordinates[neighbor_idx],
                                          init_config=theta_c[point_idx],
                                          init_point_coordinates=theta_p[point_idx],
                                          n_neighbor_pts_for_origin=self.c.n_neighbor_pts_for_origin)
            neighbor_pts.set_offsets(new_coordinates[neighbor_idx])
            new_xlim_min = min(min(new_coordinates[:, 0]), xlim_min)
            new_xlim_max = max(max(new_coordinates[:, 0]), xlim_max)
            new_ylim_min = min(min(new_coordinates[:, 1]), ylim_min)
            new_ylim_max = max(max(new_coordinates[:, 1]), ylim_max)
            ax0.set_xlim(new_xlim_min - 10, new_xlim_max + 10)
            ax0.set_ylim(new_ylim_min - 10, new_ylim_max + 10)

            tool_line_scaled.set_offsets(new_coordinates)
            draw_frame_2d(ax=None, config=theta_c[point_idx], scale=20, alpha=0.3, arrow_list=init_frame_arrow_list)
            draw_frame_2d(ax=None, config=new_config, scale=20, alpha=0.3, arrow_list=new_frame_arrow_list)

        scale_x_slider.on_changed(update)
        scale_y_slider.on_changed(update)
        point_idx_slider.on_changed(update)
        plt.show()

    def viz_frame_matching_3d(self, obj, theta_c: np.ndarray, theta_d: np.ndarray, theta_p: np.ndarray,
                              real_traj: np.ndarray = None):
        """
        to visualize the optimization result of local frame matching from the primitive shape to the real demonstration data

        Args:
            obj: the real object
            theta_c: the initial frame configuration array
            theta_d: the initial index array of neighboring points
            theta_p: the initial coordinate array of the neighboring points
            real_traj: the trajectory extracted from demonstration (J, T, N, 2)
        """
        offset = np.array([250., 0., 0.])
        scaled_point_coordinates_init = copy.deepcopy(obj.get_scaled_sample_point_coordinates(self.c.n_sample_pts_max))
        n_sample_pts = obj.n_sample_pts
        if real_traj is None:
            scaled_point_coordinates = copy.deepcopy(scaled_point_coordinates_init) + offset
        else:
            J, T, N, dim = real_traj.shape
            scaled_point_coordinates = real_traj[:, -1, 0, :] + offset  # (J, T, N, dim)

        fig = plt.figure(figsize=(16, 9))
        spec = fig.add_gridspec(ncols=1, nrows=5)
        ax0 = fig.add_subplot(spec[:4, 0], projection="3d")
        ax0.set_title("Match Local Frame from primitive shape to demo trail")
        # primitive shape, neighboring points
        patch_sample_pts_original = ax0.scatter3D(
            scaled_point_coordinates_init[:, 0], scaled_point_coordinates_init[:, 1], scaled_point_coordinates_init[:, 2],
            c='cyan', s=50, alpha=0.5, zorder=3, label="sample points")
        patch_neighbor_pts_original = ax0.scatter3D(
            scaled_point_coordinates_init[theta_d[0], 0], scaled_point_coordinates_init[theta_d[0], 1], scaled_point_coordinates_init[theta_d[0], 2],
            c='k', s=50, marker="x", alpha=1.0, zorder=5, label="neighbors")
        # scaled shape, matched neighboring points
        patch_sample_pts_scaled = ax0.scatter3D(
            scaled_point_coordinates[:, 0], scaled_point_coordinates[:, 1], scaled_point_coordinates[:, 2],
            c="limegreen", s=50, alpha=0.5, zorder=5, label="demo points")
        patch_neighbor_pts_scaled = ax0.scatter3D(
            scaled_point_coordinates[theta_d[0], 0], scaled_point_coordinates[theta_d[0], 1], scaled_point_coordinates[theta_d[0], 2],
            c='blue', s=80, marker="x", alpha=1.0, zorder=5, label="demo neighbors")
        # draw matched local frames
        arrow_scaling = 100
        init_frame_arrow_list = draw_frame_3d(ax0, theta_c[0], scale=arrow_scaling, alpha=1)
        new_frame_arrow_list = draw_frame_3d(ax0, theta_c[0], scale=arrow_scaling, alpha=1)
        plt.legend()
        set_3d_equal_auto(ax0)

        # create sliders
        scale_x_slider = Slider(ax=plt.axes([0.05, 0.23, 0.4, 0.02]), label='scale_x', valmin=0.1, valmax=10, valinit=1, valstep=0.1)
        scale_y_slider = Slider(ax=plt.axes([0.05, 0.19, 0.4, 0.02]), label='scale_y', valmin=0.1, valmax=10, valinit=1, valstep=0.1)
        scale_z_slider = Slider(ax=plt.axes([0.05, 0.15, 0.4, 0.02]), label='scale_z', valmin=0.1, valmax=10, valinit=1, valstep=0.1)
        point_idx_slider = Slider(ax=plt.axes([0.05, 0.11, 0.4, 0.02]), label='idx', valmin=1, valmax=n_sample_pts, valinit=1, valstep=1)

        if real_traj is not None:
            if T > 1:
                T_slider = Slider(ax=plt.axes([0.55, 0.23, 0.4, 0.02]), label='time', valmin=1, valmax=T, valinit=T, valstep=1)
            if N > 1:
                N_slider = Slider(ax=plt.axes([0.55, 0.19, 0.4, 0.02]), label='demo', valmin=1, valmax=N, valinit=1, valstep=1)

        def update(val):
            scale = np.array([scale_x_slider.val, scale_y_slider.val, scale_z_slider.val])
            point_idx = int(point_idx_slider.val) - 1

            if real_traj is None:
                new_coordinates = obj.get_scaled_sample_point_coordinates(self.c.n_sample_pts_max).copy() + offset
            else:
                t = int(T_slider.val) - 1 if T > 1 else 0
                n = int(N_slider.val) - 1 if N > 1 else 0
                new_coordinates = real_traj[:, t, n, :] + offset

            obj.scale(scale)
            neighbor_idx = theta_d[point_idx].flatten()
            new_config = find_local_frame(dim=3, new_point_coordinates=new_coordinates[neighbor_idx],
                                          init_config=theta_c[point_idx],
                                          init_point_coordinates=theta_p[point_idx],
                                          n_neighbor_pts_for_origin=self.c.n_neighbor_pts_for_origin)
            # update patch data
            patch_sample_pts_original._offset3d = (scaled_point_coordinates_init[neighbor_idx, 0], scaled_point_coordinates_init[neighbor_idx, 1], scaled_point_coordinates_init[neighbor_idx, 2])
            patch_neighbor_pts_scaled._offsets3d = (new_coordinates[neighbor_idx, 0], new_coordinates[neighbor_idx, 1], new_coordinates[neighbor_idx, 2])

            patch_sample_pts_scaled._offsets3d = (new_coordinates[:, 0], new_coordinates[:, 1], new_coordinates[:, 2])
            patch_neighbor_pts_original._offsets3d = (scaled_point_coordinates_init[neighbor_idx, 0], scaled_point_coordinates_init[neighbor_idx, 1], scaled_point_coordinates_init[neighbor_idx, 2])

            # update frame data
            draw_frame_3d(ax=None, config=theta_c[point_idx], scale=arrow_scaling, alpha=1, collections=init_frame_arrow_list)
            draw_frame_3d(ax=None, config=new_config, scale=arrow_scaling, alpha=1, collections=new_frame_arrow_list)
            set_3d_equal_auto(ax0)

        scale_x_slider.on_changed(update)
        scale_y_slider.on_changed(update)
        scale_z_slider.on_changed(update)
        point_idx_slider.on_changed(update)
        if real_traj is not None:
            if T > 1:
                T_slider.on_changed(update)
            if N > 1:
                N_slider.on_changed(update)

        ax0.set_axis_off()
        plt.show()



