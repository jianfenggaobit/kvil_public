import os
import logging
import numpy as np
from os.path import join, isdir
from typing import List
from scipy.spatial import ConvexHull
from scipy.spatial.distance import cdist
from mpl_toolkits.mplot3d.art3d import Line3DCollection
from sklearn.neighbors import LocalOutlierFactor
from pathos.multiprocessing import ProcessingPool as Pool

from vil.utils.py.utils import load_dict_from_yaml, save_to_yaml
from vil.utils.py.visualize import set_3d_equal_auto


class RealObject:
    def __init__(self, name: str = None, path: str = "", force_redo: bool = False, remove_outlier: bool = False,
                 last_time_step_pce: bool = False):
        if name is None:
            raise ValueError(f"{name} is invalid")
        if not path or not isdir(path):
            raise ValueError(f"path: {path} is invalid")

        self.name = name
        self.inlier_index_file = join(path, "process", f"inlier_index_{self.name}.yaml")
        self.config_file = join(path, f"_scene_config.yaml")
        cfg = load_dict_from_yaml(self.config_file)
        self.last_time_step_pce = last_time_step_pce

        self.sample_pts_all = np.array(cfg["ref_coordinates"][name])
        self.descriptors_all = np.array(cfg["intersection"][name])
        _n_sample_pts = self.sample_pts_all.shape[0]

        # Note: determine inlier index
        if force_redo:
            if remove_outlier:
                inlier_index = self.local_outlier_detector()
                if len(inlier_index) == 0:
                    raise ValueError("All points are considered as outlier, please check your data")
            else:
                inlier_index = np.arange(_n_sample_pts)
        else:
            if os.path.isfile(self.inlier_index_file):
                inlier_index = np.array(load_dict_from_yaml(self.inlier_index_file)["inlier_index"])
            else:
                raise FileExistsError(f"{self.inlier_index_file} doesn't exists. First time? run with -f option")
            if len(inlier_index) == 0:
                raise IndexError("No inliers available! Is this the first time your run this program? if so, pass '-f'")

        self.inlier_index = inlier_index
        self.n_sample_pts = len(inlier_index)
        self.outlier_mask = np.ones(_n_sample_pts, dtype=bool)
        self.outlier_mask[inlier_index] = False

        # Note: compute the spatial scaling
        self.sample_pts = self.sample_pts_all[inlier_index]
        hull = ConvexHull(self.sample_pts)
        self.null_indices = hull.vertices
        hull_points = self.sample_pts[hull.vertices, :]  # Extract the points forming the hull
        self.spatial_scale = cdist(hull_points, hull_points, metric='euclidean').max()
        if "hand" in name:
            self.spatial_scale *= 3.0
        self.current_scale = np.array([1] * 3, dtype=float)

    def local_outlier_detector(self):
        if "hand" in self.name:
            return np.arange(self.sample_pts_all.shape[0])
        clf = LocalOutlierFactor(n_neighbors=30, contamination='auto')
        in_out_lier_indicator = clf.fit_predict(self.sample_pts_all)
        inlier_idx = np.where(in_out_lier_indicator == 1)[0]

        return inlier_idx

    def draw(self, actual_coordinates, ax, collections=None, color="cyan"):
        if "hand" in self.name:
            idx_of_point_for_each_lines = [[4, 3, 2, 1, 0, 5, 9, 13, 17, 0], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16], [17, 18, 19, 20]]
            poly3d = [actual_coordinates[idx_line].tolist() for idx_line in idx_of_point_for_each_lines]

            if collections is None:
                cl = ax.add_collection3d(Line3DCollection(poly3d, color=color, linewidths=5, alpha=0.3))
                return cl
            else:
                collections.set_segments(poly3d)

    def scale(self, scale: np.ndarray):
        self.current_scale = scale

    def plot_coords(self, ax, color='cyan', zorder=1, alpha=1):
        raise NotImplementedError

    def get_shape(self):
        """
        return the pts' coordinates after scaling
        """
        return self.null_indices

    def get_len(self):
        raise len(self.sample_pts)

    def get_sample_point_coordinates(self):
        return self.sample_pts

    def get_scaled_sample_point_coordinates(self, n_sample_pts_max: int = 1000):
        # ic(self.name, self.sample_pts.shape)
        if self.n_sample_pts > n_sample_pts_max:
            down_sample_idx = np.linspace(0, self.n_sample_pts-1, n_sample_pts_max).astype(int)
            return self.sample_pts[down_sample_idx] * self.current_scale
        return self.sample_pts * self.current_scale

    def get_sample_point_traj(self, origin_traj_file_list: List[str], remove_outlier: bool = True,
                              n_samples: int = 300, viz_debug: bool = False) -> np.ndarray:
        """
        only executed when force redo
        read the origin trajectories from files, transform it to the traj of all sample points in the shape (J, T, N, 3)

        J: number of sampled points, N: total number of demonstrations, T: total time steps
        """
        sample_pts_traj = []
        for file in origin_traj_file_list:
            sample_pts_traj.append(
                np.expand_dims(np.load(file).squeeze(), 2)
            )  # (J, T, 1, 3) * N

        all_traj = np.concatenate(sample_pts_traj, axis=2)  # (J, T, N, 3)
        J, T, N, dim = all_traj.shape

        if "hand" in self.name:
            save_to_yaml(dict(inlier_index=self.inlier_index.tolist()), self.inlier_index_file)
            return all_traj
        else:
            n_neighbors = 30

        outlier_pts_from_primitive_shape = self.sample_pts_all[np.where(self.outlier_mask)[0]]
        outlier_idx_in_all_traj = np.empty(0)
        if remove_outlier:
            logging.info(f"removing outliers from traj of {self.name}")

            def get_outlier_idx(data_points):
                # clf = LocalOutlierFactor(n_neighbors=n_neighbors, contamination=0.005)
                clf = LocalOutlierFactor(n_neighbors=n_neighbors, contamination="auto")
                return np.where(clf.fit_predict(data_points) == -1)[0]

            with Pool(os.cpu_count()) as p:
                if self.last_time_step_pce:
                    # all_traj (J, T, N, 3)
                    outlier_idx_in_all_traj = np.concatenate(
                        p.map(
                            get_outlier_idx,
                            all_traj[:, [-1]].transpose((1, 2, 0, 3)).reshape((-1, J, dim))
                        )
                    )
                else:
                    outlier_idx_in_all_traj = np.concatenate(
                        p.map(
                            get_outlier_idx,
                            all_traj.transpose((1, 2, 0, 3)).reshape((-1, J, dim))
                        )
                    )

            outlier_idx_in_all_traj = np.unique(outlier_idx_in_all_traj)
            self.outlier_mask[outlier_idx_in_all_traj] = True

        # get inlier index and save to file
        self.inlier_index = np.where(np.invert(self.outlier_mask))[0]
        if self.inlier_index.shape[0] > n_samples:
            idx = np.random.choice(np.arange(self.inlier_index.shape[0]), n_samples, replace=False)
            self.inlier_index = self.inlier_index[idx]
        save_to_yaml(dict(inlier_index=self.inlier_index.tolist()), self.inlier_index_file)

        # resample from the original sample list
        self.sample_pts = self.sample_pts_all[self.inlier_index]
        self.n_sample_pts = self.inlier_index.shape[0]

        # Note this is only for debug
        if viz_debug:
            import matplotlib.pyplot as plt
            fig = plt.figure()
            ax1 = fig.add_subplot(1, 1, 1, projection='3d')
            ax1.set_title("Outlier Detection")
            ax1.scatter3D(self.sample_pts_all[:, 0], self.sample_pts_all[:, 1], self.sample_pts_all[:, 2],
                          c='cyan', s=20, label="all")
            ax1.scatter3D(self.sample_pts[:, 0] + 200, self.sample_pts[:, 1], self.sample_pts[:, 2],
                          c='lightgreen', s=20, label="remaining")

            ax1.scatter3D(outlier_pts_from_primitive_shape[:, 0],
                          outlier_pts_from_primitive_shape[:, 1],
                          outlier_pts_from_primitive_shape[:, 2],
                          marker="x", c='blue', s=50, alpha=1, label="primitive_outlier")

            if remove_outlier:
                outlier_pts_from_traj = self.sample_pts_all[outlier_idx_in_all_traj]
                ax1.scatter3D(outlier_pts_from_traj[:, 0], outlier_pts_from_traj[:, 1], outlier_pts_from_traj[:, 2],
                              marker="o", facecolor="yellow", edgecolors="k", s=100, alpha=0.5, label="traj_outlier")

            set_3d_equal_auto(ax1)
            plt.legend()
            plt.show()

        return all_traj[self.inlier_index]

