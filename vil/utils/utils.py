# import logging
# import inquirer
# from typing import List
from os.path import dirname, join, abspath
from vil.utils.py.filesystem import get_ordered_files
# from vil.utils.py.interact import ask_list


def get_root_path():
    module_path = dirname(__file__)
    return abspath(join(module_path, '../..'))


def get_example_path():
    return join(get_root_path(), "examples")


def get_config_path():
    return join(get_root_path(), "config")


def get_data_path():
    return join(get_root_path(), "data")


def get_vil_path(submodule: str = ""):
    return join(get_root_path(), "vil", submodule)


def get_rgbd_file_lists(working_dir):
    image_dir = join(working_dir, 'images')
    return get_ordered_files(image_dir, ["_rgb.png"], depth="/*"), get_ordered_files(image_dir, ["_depth.png"], depth="/*")


# def write_poses_to_log(filename, poses):
#     """
#     save poses into scene/trajectory.log
#     Args:
#         filename:
#         poses:
#     """
#     with open(filename, 'w') as f:
#         for i, pose in enumerate(poses):
#             f.write('{} {} {}\n'.format(i, i, i + 1))       #? why
#
#             f.write('{0:.16f} {1:.16f} {2:.16f} {3:.16f}\n'.format(pose[0, 0], pose[0, 1], pose[0, 2], pose[0, 3]))
#             f.write('{0:.16f} {1:.16f} {2:.16f} {3:.16f}\n'.format(pose[1, 0], pose[1, 1], pose[1, 2], pose[1, 3]))
#             f.write('{0:.16f} {1:.16f} {2:.16f} {3:.16f}\n'.format(pose[2, 0], pose[2, 1], pose[2, 2], pose[2, 3]))
#             f.write('{0:.16f} {1:.16f} {2:.16f} {3:.16f}\n'.format(pose[3, 0], pose[3, 1], pose[3, 2], pose[3, 3]))
#
#
# def read_poses_from_log(traj_log):
#     """
#     read the .log file 5 lines a time, only return the pose as an array of np 4X4 matrix.
#     Args:
#         traj_log:
#
#     Returns:
#
#     """
#     import numpy as np
#
#     trans_arr = []
#
#     with open(traj_log) as f:
#         content = f.readlines()
#
#         for i in range(0, len(content), 5):
#             # line 1
#             data = list(map(float, content[i].strip().split(' ')))
#
#             ids = (int(data[0]), int(data[1]))                                                                          # format: %d (src) %d (tgt) %f (fitness)
#             fitness = data[2]
#
#             # line 2~5
#             # join:
#             # strip: clear out the space/newline in the head and tail of the string
#             # split: space/newline/tab
#             # map
#             # list
#             # np.array
#             # reshape
#             T_gt = np.array(list(map(float, (''.join(content[i + 1:i + 5])).strip().split()))).reshape((4, 4))          # format: %f x 16
#
#             trans_arr.append(T_gt)
#
#     return trans_arr


# def select_trained_model(folder: str, pattern: List[str] = None, only_one: bool = True, model_type: str = "dcn"):
#     """
#     given the folder name, e.g. 'data/trained_models/dcn', ask the user to select multiple checkpoints from the listed
#     choices in the terminal.
#     Args:
#         folder: the folder containing multiple training results, for different objects and networks
#         pattern: list of strings to filter out folder names inside the 'folder' directory
#         only_one: whether to select only one
#         model_type: either dcn or maskrcnn
#
#     Returns: A list of selected checkpoints
#
#     """
#     if pattern is None:
#         pattern = [""]
#     model_list = get_ordered_subdirs(folder, pattern=pattern, depth="/*")
#     if len(model_list) == 0:
#         logging.warning(f"No models found with pattern {pattern}")
#     selected_model_list = []
#     while True:
#         model_path = ask_list("select one model", model_list)
#
#         if model_type == "dcn":
#             date_list = get_ordered_subdirs(model_path, depth="/*")
#             model_path = ask_list("select one date", date_list)
#
#         checkpoint_list = get_ordered_files(model_path, pattern=[".pth"], depth="/*")
#         selected_model_list.append(ask_list("select one checkpoint", checkpoint_list))
#         if only_one or input("Continue Selection? press q to quite and enter to continue: ") == "q":
#             break
#
#     return selected_model_list
