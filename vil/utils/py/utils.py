import numpy as np
import yaml
import marshmallow
from datetime import datetime
from marshmallow_dataclass import class_schema
from typing import TypeVar, Dict, Union, Type
T = TypeVar('T')


def sample_uniform_joint(config, kc):
    """
    sample a joint configuration from a uniform distribution in the joint controlable range
    Args:
        config: joint limit configuration of the robot
        kc: the kinematic chain

    Returns: sampled joint configuration

    """
    qpos = np.zeros(len(config.position_limit))
    for i in range(len(config.position_limit)):
        limits = config.position_limit[kc.joint_list[i]]
        qpos[i] = np.random.uniform(limits[0], limits[1], 1)
    return qpos


def sample_gaussian_joint(config, kc):
    """
    sample a joint configuration from a gaussian distribution, whose mean is the center of the joint range and
    the scale is the half range.
    Args:
        config: joint limit configuration of the robot
        kc: the kinematic chain

    Returns: sampled joint configuration

    """
    qpos = np.zeros(len(config.position_limit))
    for i in range(len(config.position_limit)):
        limits = config.position_limit[kc.joint_list[i]]
        qpos[i] = 0.5 * (np.random.normal(size=1) * (limits[1] - limits[0]) + np.sum(limits))
    return qpos


def load_dict_from_yaml(filename):
    with open(filename) as f:
        return yaml.load(f, Loader=yaml.SafeLoader)


def save_to_yaml(data, filename, flush=False):
    with open(filename, 'w') as outfile:
        yaml.safe_dump(data, outfile, encoding='utf-8', allow_unicode=True, default_flow_style=None, sort_keys=False)
        if flush:
            outfile.flush()


def load_dataclass_from_yaml(dataclass_type: Type[T], filename: str, include_unknown: bool = False) -> T:
    loaded = load_dict_from_yaml(filename)
    return load_dataclass_from_dict(dataclass_type, loaded, include_unknown)


def load_dataclass_from_dict(dataclass_type: Type[T], dic: Dict, include_unknown: bool = False) -> T:
    schema = class_schema(dataclass_type)
    include_schema = marshmallow.INCLUDE if include_unknown else marshmallow.EXCLUDE
    return schema().load(dic, unknown=include_schema)


def dump_data_to_dict(dataclass_type: T, data):
    schema = class_schema(dataclass_type)
    return schema().dump(data)


def dump_data_to_yaml(dataclass_type: T, data, filename: str):
    save_to_yaml(dump_data_to_dict(dataclass_type, data), filename)


def load_dataclass(dataclass_type: Type[T], config: Union[str, Dict], include_unknown: bool = False) -> T:
    if isinstance(config, str):
        return load_dataclass_from_yaml(dataclass_type, config, include_unknown)
    elif isinstance(config, Dict):
        return load_dataclass_from_dict(dataclass_type, config, include_unknown)
    else:
        raise RuntimeError(f"type of config {type(config)} is not supported, please use yaml configuration"
                           f"file or configuration dictionaries.")


def get_datetime_string(time_format: str = "%Y%m%d_%H%M%S"):
    return datetime.now().strftime(time_format)


