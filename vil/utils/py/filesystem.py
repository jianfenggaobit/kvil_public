import logging
import os
import glob
import shutil
from pathlib import Path
from natsort import natsorted


def get_ordered_subdirs(dir_name: str, pattern=None, ex_pattern=None, recursive=False, depth="/**/*"):
    """
    Args:
        dir_name (str): the directory name to look for sub-directories
        pattern (List(str)): the pattern to follow when filtering the sub-directory name
        recursive (bool): whether to search recursively
        depth (str): specify the depth to look for files
        ex_pattern: (List(str)): exclude pattern to filter the file name

    Returns:
        A list of oredered sub-directories in the dir_name that fullfils the pattern

    """
    if pattern is None:
        pattern = ['']
    if ex_pattern is None:
        ex_pattern = []

    return natsorted(filter(
        lambda x: os.path.isdir(x) and all([p in x for p in pattern]) and all([p not in x for p in ex_pattern]),
        glob.glob(dir_name + depth, recursive=recursive)
    ))


def get_ordered_files(dir_name: str, pattern=None, ex_pattern=None, recursive=False, depth="/**/*"):
    """
    Args:
        dir_name (str): the directory name to look for sub-directories
        pattern (List(str)): the pattern to follow when filtering the sub-directory name
        recursive (bool): whether to search recursively
        depth (str): specify the depth to look for files
        ex_pattern: (List(str)): exclude pattern to filter the file name

    Returns:
        A list of oredered filenames in the dir_name that fullfils the pattern

    """
    if pattern is None:
        pattern = ['']
    if ex_pattern is None:
        ex_pattern = []

    return natsorted(filter(
        lambda x: os.path.isfile(x) and all([p in x for p in pattern]) and all([p not in x for p in ex_pattern]),
        glob.glob(dir_name + depth, recursive=recursive)
    ))


def create_path(path, remove_existing=False, force=False):
    """
    check if dir exist, otherwise create new dir
    Args:
        path: the absolute path to be created. If already exists, don't do anything
        remove_existing: whether to remove the existing path before create a new one
        force: force to remove the existing path, without user feedback

    Returns:

    """
    p = Path(path)
    if remove_existing and p.exists():
        logging.warning(f"Attempt to remove {p}?")
        if force or input("delete y/N? ") == 'y':
            shutil.rmtree(p)
        else:
            raise ValueError("code try to remove path, but user terminated")
    p.mkdir(parents=True, exist_ok=True)
    return str(p.resolve(path))


def delete_path(path):
    if os.path.exists(path):
        shutil.rmtree(path)


def move_path(src, dst, remove_existing=False):
    if os.path.exists(src):
        create_path(dst, remove_existing)
    shutil.move(src, dst)


def rename_path(src, dst):
    if os.path.exists(src):
        os.rename(src, dst)


def get_root_path():
    module_path = os.path.dirname(__file__)
    return os.path.abspath(os.path.join(module_path, '..'))


def get_home():
    return Path.home()


def copy2(src, dst):
    """
    src need to be a file, while dst can be either a directory or the target filename
    """
    shutil.copy2(src, dst)


def copytree(src, dst, dirs_exist_ok=True):
    shutil.copytree(src, dst, dirs_exist_ok=dirs_exist_ok)
