import logging
import coloredlogs
from icecream import install, ic


install()
ic.configureOutput(includeContext=True)
coloredlogs.install(fmt='%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s | %(lineno)d] %(message)s',
                    level=logging.INFO,
                    datefmt='%Y-%m-%d:%H:%M:%S')