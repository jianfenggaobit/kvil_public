import click
from vil.kvil import KVIL, join
from vil.utils.utils import get_data_path


@click.command(context_settings=dict(help_option_names=['-h', '--help']))
@click.option("--domain",       "-d",   type=click.Choice(['real', 'toy'], case_sensitive=False), help="domain of the evaluation")
@click.option("--path",         "-p",   type=str,      default="",    help="the path to demonstration recordings")
@click.option("--viz",          "-v",   is_flag=True,                 help="whether to visualize constraints")
@click.option("--viz_debug",    "-vd",  is_flag=True,                 help="whether to visualize intermediate results")
@click.option("--force_redo",   "-f",   is_flag=True,                 help="whether to force redo everything")
@click.option("--load",         "-l",   is_flag=True,                 help="whether to load results")
@click.option("--time_steps",   "-t",   type=int, default=0,          help="how many time steps do you want to visualize, default=2")
def main(domain, path, force_redo, viz, viz_debug, load, time_steps):
    path = join(get_data_path(), f"demo/{domain}/{path}")
    kvil = KVIL(record_path=path, force_redo=force_redo, viz=viz, viz_debug=viz_debug, time_steps=time_steps)
    if load:
        kvil.load()
    else:
        kvil.pce()


if __name__ == "__main__":
    main()
