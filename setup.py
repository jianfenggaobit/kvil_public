from setuptools import setup, find_packages
setup(
    name='visual-imitation-learning',
    version='1.0.0',
    author='Jianfeng Gao',
    author_email='jianfeng.gao@kit.edu',
    description="This packages contains modules for keypoint-based visual imitation learning",
    packages=find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License (GPL)",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=[
        'click',
        'numpy',
        'matplotlib',
        'marshmallow',
        'marshmallow_dataclass',
        'scipy',
        'pathos',
        'sklearn',
        'natsort',
        'coloredlogs',
        'icecream',
        'pyyaml',
        # 'paramiko',  # required for ssh connection, copy files, etc
        # 'scp',
        'opencv-python==4.2.0.32',
    ]
)
